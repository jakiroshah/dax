################################################################################################
# Created: 31st December, 2017
# Author : Aalap Shah
# E-mail : ashah78@jhu.edu
################################################################################################
# Acts as the communication medium between model and the view.
#
# This communicates with the view as well as the model
################################################################################################

import random
import string

import view.main_viewer

import model.data_base
import model.data_library


def generate_new_id():
    tag1_contents = "ABCDEFGH1234"
    tag1 = ''.join(random.sample(tag1_contents, 3))
    tag2_contents = "IJKLMNOP5678"
    tag2 = ''.join(random.sample(tag2_contents, 3))
    tag3_contents = "90YZ"
    tag3 = ''.join(random.sample(tag3_contents, 2))
    tag4_contents = "QRSTUVWX"
    tag4 = ''.join(random.sample(tag4_contents, 2))

    tag = tag1 + tag2 + tag3 + tag4

    while model.data_library.validate_id(tag) == model.data_base.Msg.ERROR:
        tag = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))

    model.data_library.upload_id(tag)

    return tag


def generate_output_file(filename):
    model.data_library.generate_output_file(filename)


def upload_trial_data(filename, row_data):
    if model.data_library.upload_trial_data(filename, row_data) == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Output file not found. Please re-start the game...")


# Indicates that the done button of the start screen is pressed after basic input data validation
def set_user_input(directory_path, user_id):

    if init_model(user_id, directory_path) == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Invalid inputs. Please re-start the game...")
        return False
    return True


def set_home_directory(directory_path):
    if model.data_library.set_home_directory(directory_path) == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Invalid directory path. Please re-enter the path to game directory.")
        return False
    return True


def set_carrier_filename(carrier_filename):
    if model.data_library.set_carrier_filename(carrier_filename) == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Invalid carrier entry. Please restart the game.")
        return False
    return True


def set_image_parameters(image_parameters):
    model.data_base.GameData.image_parameters = image_parameters


def get_home_directory():
    return model.data_base.GameData.home_directory


def get_user_data():
    return model.data_base.UserInfo


def get_carrier_filenames():
    if get_sound_bank() == model.data_base.Msg.ERROR:
        return []

    carrier_filenames = []
    for row in model.data_base.GameData.carriers_bank:
        if row[1] not in carrier_filenames:
            carrier_filenames.append(row[1])

    return carrier_filenames


def get_image_parameters():
    sources = [model.data_base.ImageClassification.SOURCE_HALBERDA,
               model.data_base.ImageClassification.SOURCE_GOOGLE,
               model.data_base.ImageClassification.SOURCE_BOSS]

    dimension = [model.data_base.ImageClassification.TWO_DIMENSION,
                 model.data_base.ImageClassification.THREE_DIMENSION]

    types = [model.data_base.ImageClassification.COMPUTER_CARTOON,
             model.data_base.ImageClassification.RENDERED,
             model.data_base.ImageClassification.DRAWING,
             model.data_base.ImageClassification.REAL,
             model.data_base.ImageClassification.CARTOON]

    shadow = [model.data_base.ImageClassification.SHADOW]

    shading = [model.data_base.ImageClassification.SHADING]

    color = [model.data_base.ImageClassification.COLORED,
             model.data_base.ImageClassification.GREY_SCALE,
             model.data_base.ImageClassification.BLACK_WHITE]

    model.data_base.GameData.image_parameters = [sources, dimension, types, shadow, shading, color]

    return [sources, dimension, types, shadow, shading, color]


def get_transitions_file_paths():

    home_directory = model.data_base.GameData.home_directory
    sounds_directory = model.data_base.GameData.sounds_directory

    if get_sound_bank() == model.data_base.Msg.ERROR:
        return []

    transitions_filenames = []
    for row in model.data_base.GameData.transitions_bank:
        if row[0] not in transitions_filenames:
            file_path = home_directory + sounds_directory + str(row[0])
            transitions_filenames.append(file_path)

    return transitions_filenames


def get_online_data(user_id):

    if model.data_library.load_online_data(user_id) == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("This user was not found in the database. Please reload 'online' file.")
        return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def get_known_data():

    if model.data_library.load_known_data() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in loading the known data. Please restart the game.")
        return model.data_base.Msg.ERROR

    if model.data_library.validate_known_data() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in validating the known data. Please restart the game.")
        return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def get_sound_bank():

    if model.data_library.load_sound_bank() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in loading the sound bank. Please restart the game.")
        return model.data_base.Msg.ERROR

    if model.data_library.validate_sound_bank() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in validating the sound bank. Please check data files.")
        return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def get_known_bank():

    if model.data_library.load_known_bank() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in loading the known bank. Please restart the game.")
        return model.data_base.Msg.ERROR

    if model.data_library.validate_known_bank() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in validating the known bank. Please check data files.")
        return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def get_novel_bank():

    if model.data_library.load_novel_bank() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in loading the novel library. Please restart the game.")
        return model.data_base.Msg.ERROR

    if model.data_library.validate_novel_bank() == model.data_base.Msg.ERROR:
        view.main_viewer.error_gui("Error in validating the novel library. Please check data files.")
        return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def get_online_cross_known():

    known_data = model.data_base.GameData.known_data
    online_data = model.data_base.GameData.online_data

    online_cross_known = []
    for row in known_data:
        for i in online_data:
            if row[0].lower() == i.lower():
                online_cross_known.append(row)
                break

    return online_cross_known


def check_sources_list(sources_list, bank_row):
    if sources_list[0] and sources_list[0] == int(bank_row[2]):
        return True
    if sources_list[1] and sources_list[1] == int(bank_row[3]):
        return True
    if sources_list[2] and sources_list[2] == int(bank_row[4]):
        return True
    return False


def check_dimension_list(dimension_list, bank_row):
    if dimension_list[0]:
        if bank_row[5] == "2D":
            return True
    elif dimension_list[1]:
        if bank_row[5] == "3D":
            return True
    return False


def check_types_list(types_list, bank_row):
    if types_list[0] and types_list[0] == int(bank_row[6]):
        return True
    if types_list[1] and types_list[1] == int(bank_row[7]):
        return True
    if types_list[2] and types_list[2] == int(bank_row[8]):
        return True
    if types_list[3] and types_list[3] == int(bank_row[9]):
        return True
    if types_list[4] and types_list[4] == int(bank_row[10]):
        return True
    return False


def check_shadow_list(shadow_list, bank_row):
    if shadow_list[0] == int(bank_row[12]):
        return True
    return False


def check_shading_list(shading_list, bank_row):
    if shading_list[0] == int(bank_row[13]):
        return True
    return False


def check_colors_list(colors_list, bank_row):
    if colors_list[0] and colors_list[0] == int(bank_row[6]):
        return True
    elif colors_list[1] or colors_list[2]:
        if colors_list[6]:
            return True
    return False


def run_check_known_bank():

    sound_bank = model.data_base.GameData.sound_bank
    known_bank = model.data_base.GameData.known_bank

    known_image_data = []
    for i in known_bank:
        image = model.data_base.ImageData(i[0].lower(), 0.0, i[1], "", model.data_base.ImageType.KNOWN)
        known_image_data.append(image)

    flag = False
    for i in known_image_data:
        for j in sound_bank:
            if i.get_object_name() == j[0].lower():
                sounds = list()
                sounds.append(j[1])
                if len(j) > 2:
                    sounds.append(j[2])
                if len(j) > 3:
                    sounds.append(j[3])
                i.set_sound_filename(sounds)
                flag = True
                break
        if not flag:
            print(i.get_object_name() + ": known sound file not found")
            known_image_data.remove(i)
        flag = False

    return known_image_data


def run_check_novel_bank():

    sound_bank = model.data_base.GameData.sound_bank
    novel_bank = model.data_base.GameData.novel_bank

    novel_image_data = []
    for i in novel_bank:
        image = model.data_base.ImageData(i[0].lower(), 0.0, i[1], "", model.data_base.ImageType.NOVEL)
        novel_image_data.append(image)

    flag = False
    for i in novel_image_data:
        for j in sound_bank:
            if i.get_object_name() == j[0].lower():
                sounds = list()
                sounds.append(j[1])
                if len(j) > 2:
                    sounds.append(j[2])
                if len(j) > 3:
                    sounds.append(j[3])
                i.set_sound_filename(sounds)
                flag = True
                break
        if not flag:
            print(i.get_object_name() + ": novel sound file not found")
            novel_image_data.remove(i)
        flag = False

    return novel_image_data


def filter_known_bank(use_online_data):

    if use_online_data:
        known_data = get_online_cross_known()
    else:
        known_data = model.data_base.GameData.known_data

    sound_bank = model.data_base.GameData.sound_bank
    known_bank = model.data_base.GameData.known_bank

    known_image_data = []

    # TODO: Add additional conditions here
    sources_list = model.data_base.GameData.image_parameters[0]
    dimensions_list = model.data_base.GameData.image_parameters[1]
    types_list = model.data_base.GameData.image_parameters[2]
    shadow_list = model.data_base.GameData.image_parameters[3]
    shading_list = model.data_base.GameData.image_parameters[4]
    colors_list = model.data_base.GameData.image_parameters[5]

    for i in known_data:
        image_is_found = False
        for j in known_bank:
            if i[0].lower() == j[0].lower():

                is_valid = check_sources_list(sources_list, j)
                if not is_valid:
                    continue

                is_valid = is_valid and check_dimension_list(dimensions_list, j)
                if not is_valid:
                    continue

                is_valid = is_valid and check_types_list(types_list, j)
                if not is_valid:
                    continue

                is_valid = is_valid and check_shadow_list(shadow_list, j)
                if not is_valid:
                    continue

                is_valid = is_valid and check_shading_list(shading_list, j)
                if not is_valid:
                    continue

                is_valid = is_valid and check_colors_list(colors_list, j)
                if not is_valid:
                    continue

                image = model.data_base.ImageData(i[0].lower(), i[1], j[1], "", model.data_base.ImageType.KNOWN)
                known_image_data.append(image)

                image_is_found = True
                break

        if not image_is_found:
            for j in known_bank:
                if i[0].lower() == j[0].lower():
                    image = model.data_base.ImageData(i[0].lower(), i[1], j[1], "", model.data_base.ImageType.KNOWN)
                    known_image_data.append(image)
                    break

    flag = False
    for i in known_image_data:
        for j in sound_bank:
            if i.get_object_name() == j[0].lower():
                sounds = list()
                sounds.append(j[1])
                if len(j) > 2:
                    sounds.append(j[2])
                if len(j) > 3:
                    sounds.append(j[3])
                i.set_sound_filename(sounds)
                flag = True
                break
        if not flag:
            print(i.get_object_name() + ": known sound file not found")
            view.main_viewer.error_gui("Sound file for a known object not found. Please check data files.")
            return model.data_base.Msg.ERROR
        flag = False

    return known_image_data


def filter_novel_bank():

    length = 24

    sound_bank = model.data_base.GameData.sound_bank
    novel_bank = model.data_base.GameData.novel_bank

    novel_image_data = []

    if len(novel_bank) < length:
        view.main_viewer.error_gui("Not enough novel images found in the novel bank. This is weird!")
        return model.data_base.Msg.ERROR

    temp_list = []
    while not len(novel_image_data) == length:
        index = random.randint(0, len(novel_bank) - 1)
        obj_name = novel_bank[index][0].lower()

        while obj_name in temp_list:
            index = random.randint(0, len(novel_bank) - 1)
            obj_name = novel_bank[index][0].lower()

        image = model.data_base.ImageData(obj_name, 0.0, novel_bank[index][1], "", model.data_base.ImageType.NOVEL)

        temp_list.append(obj_name)
        novel_image_data.append(image)

    flag = False
    for i in novel_image_data:
        for j in sound_bank:
            if i.get_object_name() == j[0].lower():
                sounds = list()
                sounds.append(j[1])
                if len(j) > 2:
                    sounds.append(j[2])
                if len(j) > 3:
                    sounds.append(j[3])
                i.set_sound_filename(sounds)
                flag = True
                break
        if not flag:
            print(i.get_object_name() + ": novel sound file not found")
            view.main_viewer.error_gui("Sound file for a novel object not found. Please check data files.")
            return model.data_base.Msg.ERROR
        flag = False

    return novel_image_data


def create_new_trial(img1, img2):

    target_name = img1.get_object_name()
    other_name = img2.get_object_name()

    print(target_name, other_name)

    target_freq = img1.get_frequency()
    filename1 = img1.get_image_filename()
    filename2 = img2.get_image_filename()
    sound = img1.get_sound_filename()
    image_type = img1.get_image_type() + "_" + img2.get_image_type()

    return model.data_base.TrialData(target_name, other_name, target_freq, filename1, filename2, sound, image_type)


def get_practice_known_data():

    practice_known_data = model.data_base.GameData.practice_known_data
    known_bank = model.data_base.GameData.known_bank
    sound_bank = model.data_base.GameData.sound_bank

    practice_trials = []

    if len(practice_known_data) < 4:
        view.main_viewer.error_gui("Not enough practice known words! Using Mommy-Daddy trial.")

        img_file1 = "mommy_g.png"
        img_sound1 = ["mommy_1.wav", "mommy_2.wav"]
        image1 = model.data_base.ImageData("mommy", "0.0", img_file1, img_sound1, model.data_base.ImageType.KNOWN)
        practice_trials.append(image1)

        img_file2 = "daddy_g.png"
        img_sound2 = ["daddy_1.wav", "daddy_2.wav"]
        image2 = model.data_base.ImageData("daddy", "0.0", img_file2, img_sound2, model.data_base.ImageType.KNOWN)
        practice_trials.append(image2)

        # TODO: Need to change this part to return the two after adding respective file names.
        # return model.data_base.Msg.ERROR

    else:
        # TODO: Add additional conditions here
        for i in practice_known_data:
            for j in known_bank:
                if i[0].lower() == j[0].lower():
                    image = model.data_base.ImageData(i[0].lower(), i[1], j[1], "", model.data_base.ImageType.KNOWN)
                    practice_trials.append(image)
                    break

    flag = False
    for i in practice_trials:
        for j in sound_bank:
            if i.get_object_name() == j[0].lower():
                sounds = list()
                sounds.append(j[1])
                if len(j) > 2:
                    sounds.append(j[2])
                if len(j) > 3:
                    sounds.append(j[3])
                i.set_sound_filename(sounds)
                flag = True
                break
        if not flag:
            view.main_viewer.error_gui("Sound file for a practice known object not found. Please check data files.")
            return model.data_base.Msg.ERROR
        flag = False

    return practice_trials


def get_practice_trials(practice_known_data):

    home_directory = model.data_base.GameData.home_directory
    images_directory = model.data_base.GameData.images_directory
    sounds_directory = model.data_base.GameData.sounds_directory

    final_list = []

    if len(practice_known_data) == 2:

        img1 = practice_known_data[0]
        img2 = practice_known_data[1]

        trial_data = create_new_trial(img1, img2)
        final_list.append(trial_data)

        trial_data = create_new_trial(img2, img1)
        final_list.append(trial_data)

    else:
        temp_ind = random.sample(range(0, len(practice_known_data)), 4)

        img1 = practice_known_data[temp_ind[0]]
        img2 = practice_known_data[temp_ind[1]]
        img3 = practice_known_data[temp_ind[2]]
        img4 = practice_known_data[temp_ind[3]]

        trial1 = create_new_trial(img1, img2)
        final_list.append(trial1)

        trial2 = create_new_trial(img3, img4)
        final_list.append(trial2)

    block_trial_list = []

    left_image_filename1 = str(final_list[0].get_target_filename())
    right_image_filename1 = str(final_list[0].get_other_filename())
    sound_list1 = final_list[0].get_target_sound()

    left_image_path = home_directory + images_directory + left_image_filename1
    right_image_path = home_directory + images_directory + right_image_filename1
    sounds_path_list = []

    carrier_filename = model.data_base.GameData.carrier_filename
    sound_path_1 = home_directory + sounds_directory + str(carrier_filename)
    sounds_path_list.append(sound_path_1)
    if len(sound_list1) == 1:
        sound_path_2 = home_directory + sounds_directory + str(sound_list1[0])
        sounds_path_list.append(sound_path_2)
    if len(sound_list1) > 1:
        sound_path_2 = home_directory + sounds_directory + str(sound_list1[0])
        sounds_path_list.append(sound_path_2)
        sound_path_3 = home_directory + sounds_directory + str(sound_list1[1])
        sounds_path_list.append(sound_path_3)

    block = model.data_base.BlockData(left_image_path, right_image_path, sounds_path_list)
    block_trial_list.append(block)

    left_image_filename2 = str(final_list[1].get_target_filename())
    right_image_filename2 = str(final_list[1].get_other_filename())
    sound_list2 = final_list[1].get_target_sound()

    left_image_path = home_directory + images_directory + left_image_filename2
    right_image_path = home_directory + images_directory + right_image_filename2
    sounds_path_list = []

    carrier_filename = model.data_base.GameData.carrier_filename
    sound_path_1 = home_directory + sounds_directory + str(carrier_filename)
    sounds_path_list.append(sound_path_1)
    if len(sound_list2) == 1:
        sound_path_2 = home_directory + sounds_directory + str(sound_list2[0])
        sounds_path_list.append(sound_path_2)
    if len(sound_list2) > 1:
        sound_path_2 = home_directory + sounds_directory + str(sound_list2[0])
        sounds_path_list.append(sound_path_2)
        sound_path_3 = home_directory + sounds_directory + str(sound_list2[1])
        sounds_path_list.append(sound_path_3)

    block = model.data_base.BlockData(left_image_path, right_image_path, sounds_path_list)
    block_trial_list.append(block)

    return block_trial_list


def set_known_per_block(known_data_size):

    # total number of known images for a full version of the game
    model.data_base.GameData.known_per_block = 18  # DEFAULT VALUE
    min_known_images = model.data_base.GameData.known_per_block * 2

    if known_data_size < min_known_images:

        total_num_of_kk_trials = int(known_data_size / 12)
        if total_num_of_kk_trials == 0 and not known_data_size == 12:
            model.data_base.GameData.known_per_block = 6
            model.data_base.GameData.is_player_data_ignored = True
            print("Not enough known words identified. Using the frequency data to generate trials...")
            return model.data_base.Msg.ERROR
        else:
            model.data_base.GameData.known_per_block = int(known_data_size / 2)

    model.data_base.GameData.is_player_data_ignored = False
    return model.data_base.Msg.SUCCESS


def get_kk_nk_kn_dist():

    known_data_size = model.data_base.GameData.known_per_block

    KNOWN = "KNOWN"
    NOVEL = "NOVEL"

    trial_types = []

    num_trials_extra = known_data_size % 6
    num_trials_in_ratio = int(known_data_size / 6)

    for i in range(num_trials_in_ratio):
        # ---------------------- KNOWN - KNOWN -----------------------------
        img1_type = KNOWN
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

        # ---------------------- KNOWN - KNOWN -----------------------------
        img1_type = KNOWN
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

        # ---------------------- NOVEL - KNOWN -----------------------------
        img1_type = NOVEL
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

        # ---------------------- KNOWN - NOVEL -----------------------------
        img1_type = KNOWN
        img2_type = NOVEL

        trial_types.append(img1_type + "_" + img2_type)

    if num_trials_extra >= 1:
        # ---------------------- NOVEL - KNOWN -----------------------------
        img1_type = NOVEL
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

    if num_trials_extra >= 2:
        # ---------------------- NOVEL - KNOWN -----------------------------
        img1_type = NOVEL
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

    if num_trials_extra == 3:
        # ---------------------- KNOWN - NOVEL -----------------------------
        img1_type = KNOWN
        img2_type = NOVEL

        trial_types.append(img1_type + "_" + img2_type)

    if num_trials_extra == 4:
        # ---------------------- KNOWN - KNOWN -----------------------------
        img1_type = KNOWN
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

    if num_trials_extra == 5:
        # ---------------------- KNOWN - NOVEL -----------------------------
        img1_type = KNOWN
        img2_type = NOVEL

        trial_types.append(img1_type + "_" + img2_type)

        # ---------------------- KNOWN - KNOWN -----------------------------
        img1_type = KNOWN
        img2_type = KNOWN

        trial_types.append(img1_type + "_" + img2_type)

    # ---------------------- RANDOMIZE -----------------------------
    final_trial_types = randomize_by_trial_type(trial_types)

    while final_trial_types == model.data_base.Msg.ERROR:
        final_trial_types = randomize_by_trial_type(trial_types)

    return final_trial_types


def randomize_by_trial_type(trial_types):

    temp_list = trial_types[:]
    final_list = []

    rep_check = ["", ""]
    tries = 0
    while not len(final_list) == len(trial_types):
        index = random.randint(0, len(temp_list) - 1)
        trial = temp_list[index]
        while rep_check[0] == str(trial) and rep_check[1] == str(trial):
            index = random.randint(0, len(temp_list) - 1)
            trial = temp_list[index]
            if tries >= 1000:
                print("Oops, could not randomize list by trial type!")
                return model.data_base.Msg.ERROR
            tries = tries + 1
        rep_check.pop(0)
        rep_check.append(trial)
        final_list.append(temp_list[index])
        temp_list.pop(index)

    return final_list


def get_frequency_type(image_data):

    LOW = "LOW"
    MID = "MID"
    HIGH = "HIGH"

    freq_low = model.data_base.GameData.low_freq_lower
    freq_mid = model.data_base.GameData.mid_freq_lower
    freq_h_l = model.data_base.GameData.high_freq_lower
    freq_h_u = model.data_base.GameData.high_freq_upper

    freq_type = ""

    if freq_low <= float(image_data.get_frequency()) < freq_mid:
        freq_type = LOW
    elif freq_mid <= float(image_data.get_frequency()) < freq_h_l:
        freq_type = MID
    elif freq_h_l <= float(image_data.get_frequency()) <= freq_h_u:
        freq_type = HIGH

    return freq_type


def populate_blocks(known_data, novel_data, trial_types1, trial_types2):

    length = model.data_base.GameData.known_per_block * 2

    if len(known_data) < length:
        view.main_viewer.error_gui("Not enough known words to make the game! Duplicating data!")
        print("Not enough known words to make the game, duplicating data!")
        return model.data_base.Msg.ERROR

    if not len(trial_types1) == len(trial_types2):
        return model.data_base.Msg.ERROR

    # Try to get equal representation of all the frequencies for the known data set
    filtered_known_data = []
    low_frequency = []
    mid_frequency = []
    high_frequency = []
    for image_data in known_data:
        freq_type = get_frequency_type(image_data)
        if freq_type == "LOW":
            low_frequency.append(image_data)
        elif freq_type == "MID":
            mid_frequency.append(image_data)
        elif freq_type == "HIGH":
            high_frequency.append(image_data)

    sorted_list = [low_frequency, mid_frequency, high_frequency]
    sorted_list.sort(key=len)

    num = int(length / 3)

    if len(sorted_list[0]) >= num:
        indices = random.sample(range(0, len(sorted_list[0])), num)
        for i in indices:
            filtered_known_data.append(sorted_list[0][i])

        indices = random.sample(range(0, len(sorted_list[1])), num)
        for i in indices:
            filtered_known_data.append(sorted_list[1][i])

        indices = random.sample(range(0, len(sorted_list[2])), length - len(filtered_known_data))
        for i in indices:
            filtered_known_data.append(sorted_list[2][i])
    else:
        filtered_known_data.extend(sorted_list[0])

        if len(sorted_list[1]) + len(sorted_list[0]) < 2 * num:
            filtered_known_data.extend(sorted_list[1])
        else:
            indices = random.sample(range(0, len(sorted_list[1])), num)
            for i in indices:
                filtered_known_data.append(sorted_list[1][i])

        if len(sorted_list[2]) <= length - len(filtered_known_data):
            filtered_known_data.extend(sorted_list[2])
        else:
            indices = random.sample(range(0, len(sorted_list[2])), length - len(filtered_known_data))
            for i in indices:
                filtered_known_data.append(sorted_list[2][i])

    tries = 0
    final_known_data = randomize_by_frequency(filtered_known_data)

    while final_known_data == model.data_base.Msg.ERROR:
        final_known_data = randomize_by_frequency(filtered_known_data)

        tries = tries + 1
        if tries >= 10:
            random.shuffle(filtered_known_data)
            final_known_data = filtered_known_data[:]
            break

    KNOWN = "KNOWN"
    NOVEL = "NOVEL"

    KNOWN_KNOWN = KNOWN + "_" + KNOWN
    NOVEL_KNOWN = NOVEL + "_" + KNOWN
    KNOWN_NOVEL = KNOWN + "_" + NOVEL

    trial_types = trial_types1 + trial_types2
    trial_list = []

    if not final_known_data:
        print("Something went wrong in final_known_data!")
        return model.data_base.Msg.ERROR

    for i in final_known_data:
        if not i:
            print("Something went wrong in final_known_data!")
            return model.data_base.Msg.ERROR

    for image_type in trial_types:
        if str(image_type) == KNOWN_KNOWN:
            if len(final_known_data) >= 2:
                temp_ind1 = [0, 1]

            img1 = final_known_data[temp_ind1[0]]
            img2 = final_known_data[temp_ind1[1]]

            trial = create_new_trial(img1, img2)

            temp_ind1.sort(reverse=True)
            final_known_data.pop(temp_ind1[0])
            final_known_data.pop(temp_ind1[1])

            trial_list.append(trial)

        elif str(image_type) == NOVEL_KNOWN:
            temp_ind1 = random.randint(0, len(novel_data) - 1)
            temp_ind2 = 0

            img1 = novel_data[temp_ind1]
            img2 = final_known_data[temp_ind2]

            trial = create_new_trial(img1, img2)

            novel_data.pop(temp_ind1)
            final_known_data.pop(temp_ind2)

            trial_list.append(trial)

        elif str(image_type) == KNOWN_NOVEL:
            temp_ind1 = 0
            temp_ind2 = random.randint(0, len(novel_data) - 1)

            img1 = final_known_data[temp_ind1]
            img2 = novel_data[temp_ind2]

            trial = create_new_trial(img1, img2)

            final_known_data.pop(temp_ind1)
            novel_data.pop(temp_ind2)

            trial_list.append(trial)

    return trial_list


def randomize_by_frequency(known_data):

    temp_list = known_data[:]
    final_list = []

    rep_check = ["", ""]
    tries = 0
    while not len(final_list) == len(known_data):
        index = random.randint(0, len(temp_list) - 1)
        image_data = temp_list[index]
        freq_type = get_frequency_type(image_data)

        while rep_check[0] == freq_type and rep_check[1] == freq_type:
            index = random.randint(0, len(temp_list) - 1)
            image_data = temp_list[index]
            freq_type = get_frequency_type(image_data)

            if tries >= 1000:
                print("Oops, could not randomize list by frequency!")
                return model.data_base.Msg.ERROR
            tries = tries + 1

        rep_check.pop(0)
        rep_check.append(freq_type)
        final_list.append(temp_list[index])
        temp_list.pop(index)

    return final_list


# [Input] target_indices: indicates as to on which screen the target object appears
#                         if 1, target object appears on the left screen
#                         if 0, target object appears on the right screen
def prepare_game_trials(paired_data, target_indices):

    block_trial_list = []

    home_directory = model.data_base.GameData.home_directory
    images_directory = model.data_base.GameData.images_directory
    sounds_directory = model.data_base.GameData.sounds_directory

    user_id = model.data_base.UserInfo.PLAYER_ID
    user_age = model.data_base.UserInfo.PLAYER_AGE

    output_filename = user_id + "_" + user_age + ".csv"
    generate_output_file(output_filename)

    title_row = ["Left Screen Image",
                 "Left Screen Filename",
                 "Right Screen Image",
                 "Right Screen Filename",
                 "Target Image Name",
                 "Target Image Freq",
                 "Sound File(s) Played",
                 "Image Type",
                 "Block Number",
                 "Player Data Used"]
    upload_trial_data(output_filename, title_row)

    count = 0

    for trial in paired_data:

        names = [trial.get_other_name(), trial.get_target_name()]
        images = [trial.get_other_filename(), trial.get_target_filename()]

        left_image = str(names[target_indices[count]])
        left_image_filename = str(images[target_indices[count]])

        right_image = str(names[target_indices[count] ^ 1])
        right_image_filename = str(images[target_indices[count] ^ 1])

        sound_list = trial.get_target_sound()

        target_name = str(trial.get_target_name())
        target_freq = str(trial.get_target_frequency())
        image_type = str(trial.get_image_type())

        if count < len(paired_data) / 2:
            block_number = "First Block"
        else:
            block_number = "Second Block"

        if model.data_base.GameData.is_player_data_ignored:
            player_data_used = "NO"
        else:
            player_data_used = "YES"

        row_data = [left_image,
                    left_image_filename,
                    right_image,
                    right_image_filename,
                    target_name,
                    target_freq,
                    str(sound_list),
                    image_type,
                    str(block_number),
                    str(player_data_used)]
        upload_trial_data(output_filename, row_data)

        left_image_path = home_directory + images_directory + left_image_filename
        right_image_path = home_directory + images_directory + right_image_filename
        sounds_path_list = []

        carrier_filename = model.data_base.GameData.carrier_filename
        sound_path_1 = home_directory + sounds_directory + str(carrier_filename)
        sounds_path_list.append(sound_path_1)
        if len(sound_list) == 1:
            sound_path_2 = home_directory + sounds_directory + str(sound_list[0])
            sounds_path_list.append(sound_path_2)
        if len(sound_list) > 1:
            sound_path_2 = home_directory + sounds_directory + str(sound_list[0])
            sounds_path_list.append(sound_path_2)
            sound_path_3 = home_directory + sounds_directory + str(sound_list[1])
            sounds_path_list.append(sound_path_3)

        block = model.data_base.BlockData(left_image_path, right_image_path, sounds_path_list)
        block_trial_list.append(block)

        count = count + 1

    return block_trial_list


def init_view():
    model.data_base.GameData.application = view.main_viewer.init_view()
    view.main_viewer.init_start_screen()


def init_check_screen():
    model.data_base.GameData.carrier_filename = "find_that.wav"

    # ------------------- PREPARE CHECK DATA -------------------
    if get_sound_bank() == model.data_base.Msg.ERROR:
        return

    if get_known_bank() == model.data_base.Msg.ERROR:
        return

    if get_novel_bank() == model.data_base.Msg.ERROR:
        return

    # ------------------- GET CHECK GAME DATA -------------------
    known_data = run_check_known_bank()
    novel_data = run_check_novel_bank()

    home_dir = model.data_base.GameData.home_directory
    images_dir = model.data_base.GameData.images_directory
    sounds_dir = model.data_base.GameData.sounds_directory

    all_images = []
    all_sounds = []
    for i in known_data:
        all_images.append(home_dir + images_dir + i.get_image_filename())

        sounds = list()
        sound_list = i.get_sound_filename()
        carrier_filename = model.data_base.GameData.carrier_filename
        sound_path_1 = home_dir + sounds_dir + str(carrier_filename)
        sounds.append(sound_path_1)
        if len(sound_list) == 1:
            sound_path_2 = home_dir + sounds_dir + str(sound_list[0])
            sounds.append(sound_path_2)
        if len(sound_list) > 1:
            sound_path_2 = home_dir + sounds_dir + str(sound_list[0])
            sounds.append(sound_path_2)
            sound_path_3 = home_dir + sounds_dir + str(sound_list[1])
            sounds.append(sound_path_3)
        all_sounds.append(sounds)

    for i in novel_data:
        all_images.append(home_dir + images_dir + i.get_image_filename())

        sounds = list()
        sound_list = i.get_sound_filename()
        carrier_filename = model.data_base.GameData.carrier_filename
        sound_path_1 = home_dir + sounds_dir + str(carrier_filename)
        sounds.append(sound_path_1)
        if len(sound_list) == 1:
            sound_path_2 = home_dir + sounds_dir + str(sound_list[0])
            sounds.append(sound_path_2)
        if len(sound_list) > 1:
            sound_path_2 = home_dir + sounds_dir + str(sound_list[0])
            sounds.append(sound_path_2)
            sound_path_3 = home_dir + sounds_dir + str(sound_list[1])
            sounds.append(sound_path_3)
        all_sounds.append(sounds)

    bg_screen = home_dir + images_dir + "white.jpg"

    v_app = model.data_base.GameData.application
    view.main_viewer.init_check_view(v_app, bg_screen, all_images, all_sounds)


def init_main_screen(user_id):

    # ---------------------- PREPARE GAME DATA ----------------------
    if get_sound_bank() == model.data_base.Msg.ERROR:
        return

    if get_known_bank() == model.data_base.Msg.ERROR:
        return

    if get_novel_bank() == model.data_base.Msg.ERROR:
        return

    if get_online_data(user_id) == model.data_base.Msg.ERROR:
        return

    if get_known_data() == model.data_base.Msg.ERROR:
        return

    # ---------------------- FILTER GAME DATA ----------------------
    known_data = filter_known_bank(True)
    if known_data == model.data_base.Msg.ERROR:
        return

    if set_known_per_block(len(known_data)) == model.data_base.Msg.ERROR:
        known_data = filter_known_bank(False)

    novel_data = filter_novel_bank()  # Max number of images ever required
    if novel_data == model.data_base.Msg.ERROR:
        return

    # ---------------------- PREPARE THE PRACTICE BLOCK ----------------------
    practice_known_data = get_practice_known_data()
    if practice_known_data == model.data_base.Msg.ERROR:
        return

    practice_blocks = get_practice_trials(practice_known_data)

    # ---------------------- PREPARE THE GAME BLOCKS ----------------------
    trial_types1 = get_kk_nk_kn_dist()
    trial_types2 = get_kk_nk_kn_dist()
    trial_list = populate_blocks(known_data, novel_data, trial_types1, trial_types2)
    if trial_list == model.data_base.Msg.ERROR:
        return

    # ------------------ ASSOCIATE SCREENS TO IMAGES ------------------
    rep_check = [-1, -1, -1]
    target_indices = []
    for i in range(len(trial_list)):
        rep_check[2] = random.randint(0, 1)
        while rep_check[2] == rep_check[0] and rep_check[2] == rep_check[1]:
            rep_check[2] = random.randint(0, 1)

        target_indices.append(rep_check[2])
        rep_check.pop(0)
        rep_check.append(-1)

    home_directory = model.data_base.GameData.home_directory
    images_directory = model.data_base.GameData.images_directory
    sounds_directory = model.data_base.GameData.sounds_directory

    bg_screen = home_directory + images_directory + "white.jpg"
    tr_screen = home_directory + images_directory + "clapping.gif"

    # ---------------------- PREPARE OUTPUT FILE ----------------------
    print("Writing output to file...")

    game_blocks = prepare_game_trials(trial_list, target_indices)

    # -------------------------- START GAME ---------------------------

    transition_filenames = get_transitions_file_paths()
    transition_index = random.sample(range(0, len(transition_filenames)), 2)

    tr_sound = list()
    tr_sound.append(transition_filenames[transition_index[0]])
    tr_sound.append(transition_filenames[transition_index[1]])
    tr_sound.append(home_directory + sounds_directory + "get_ready.wav")

    end_sound = list()
    end_sound.append(home_directory + sounds_directory + "super_duper.wav")
    end_sound.append(home_directory + sounds_directory + "you_did_it.wav")

    print("Starting the game...")
    v_app = model.data_base.GameData.application
    view.main_viewer.init_main_screen(v_app, bg_screen, tr_screen, practice_blocks, game_blocks, tr_sound, end_sound)


def init_model(player_id, home_directory):
    return model.data_library.set_inputs(player_id, home_directory)


if __name__ == "__main__":
    init_view()


# TODO:
# check if elements in classification are present - they may be empty ""!
