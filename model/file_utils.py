################################################################################################
# Created: 23rd December, 2017
# Author : Aalap Shah
# E-mail : ashah78@jhu.edu
################################################################################################
# Defines the CRUD operations that are done on the files that are used in the experiment
################################################################################################

import csv


# [Input] filename: name of the file that is to be created
def create_file(filename):
    file = open(filename, 'wb')
    file.close()


# [Input] filename: name of the file that is to be written on
# [Input] row_data: the entire row entry that is to be written into the file
def write_row_to_file(filename, row_data):

    try:
        file = open(filename, 'a')
        writer = csv.writer(file)
        writer.writerow(row_data)
        file.close()

    except Exception as e:
        print("Error while writing to ", filename)
        print(str(e))


# [Input]  filename: name of the file that is to be read
# [Output]     data: a list of list containing all the data
def read_from_file(filename):

    try:
        file = open(filename, 'r')
        reader = csv.reader(file)

        data = []
        for row in reader:
            data.append(row)

        file.close()

        return data

    except Exception as e:
        print("Error while reading from ", filename)
        print(str(e))


# [Input]    filename: name of the file that is to be read
# [Input]  row_number: the index(1 to N) of the row whose data is to be read
# [Output]       data: a list all the data of the requested row OR None if the row_number is out of range
def read_row_from_file(filename, row_number):

    try:
        file = open(filename, 'r')
        reader = csv.reader(file)

        for index, row in enumerate(reader, start=1):
            if index == row_number:
                file.close()
                return row

        file.close()
        return None

    except Exception as e:
        print("Error while reading from ", filename)
        print(str(e))
