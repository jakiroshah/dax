################################################################################################
# Created: 25th December, 2017
# Author : Aalap Shah
# E-mail : ashah78@jhu.edu
################################################################################################
# Uses the library folder of this package to extract data that is used in the game
#
# The images used in the game are stored in the 'images' folder in /library
# The sounds used in the game are stored in the 'sounds' folder in /library
# The /library contains files which specify the data to be used for the current player
################################################################################################

import os
import model.data_base
import model.file_utils


# Verifies and saves the inputs entered by the user
# [Input]      player_id: the string representing the ID assigned to the player
# [Input] home_directory: a string describing the location of the root folder of this code on the computer
def set_inputs(player_id, home_directory):

    model.data_base.UserInfo.PLAYER_ID = player_id

    '''
    Check if the default home directory exists, or prompt the user to change
    '''
    if not (os.path.isdir(home_directory)):
        print("The entered home directory does not exist. Please re-enter the path to game directory.")
        return model.data_base.Msg.ERROR
    else:
        model.data_base.GameData.home_directory = home_directory
        library_directory = model.data_base.GameData.library_directory

        filename = home_directory + library_directory + "/players_info.csv"
        data = model.file_utils.read_from_file(filename)

        if not data:
            return model.data_base.Msg.ERROR

        for row in data:
            if row[0] == player_id:
                return model.data_base.Msg.SUCCESS

    return model.data_base.Msg.ERROR


def set_home_directory(home_directory):
    if not (os.path.isdir(home_directory)):
        print("The entered home directory does not exist. Please re-enter the path to game directory.")
        return model.data_base.Msg.ERROR

    model.data_base.GameData.home_directory = home_directory
    return model.data_base.Msg.SUCCESS


def set_carrier_filename(carrier_filename):

    home_directory = model.data_base.GameData.home_directory
    sounds_directory = model.data_base.GameData.sounds_directory

    sound_path_primary = home_directory + sounds_directory + str(carrier_filename)

    if not (os.path.exists(sound_path_primary)):
        print("One of the primary sounds is not found. Validation failed.")
        return model.data_base.Msg.ERROR

    model.data_base.GameData.carrier_filename = carrier_filename

    return model.data_base.Msg.SUCCESS


def load_sound_bank():

    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if home_directory == "":
        print("Home directory not specified, please enter the path to game directory.")
        return model.data_base.Msg.ERROR

    '''
    Load the bank of known images present in the data base.
    Remove the title row of the file.
    '''
    filename_bank = home_directory + library_directory + "/data_bank_sounds.csv"
    data_bank = model.file_utils.read_from_file(filename_bank)

    if not data_bank:
        return model.data_base.Msg.ERROR

    model.data_base.GameData.sound_bank = data_bank[1:]

    filename_bank = home_directory + library_directory + "/data_bank_carriers.csv"
    data_bank = model.file_utils.read_from_file(filename_bank)

    if not data_bank:
        return model.data_base.Msg.ERROR

    model.data_base.GameData.carriers_bank = data_bank[1:]

    filename_bank = home_directory + library_directory + "/data_bank_transitions.csv"
    data_bank = model.file_utils.read_from_file(filename_bank)

    if not data_bank:
        return model.data_base.Msg.ERROR

    model.data_base.GameData.transitions_bank = data_bank[1:]

    return model.data_base.Msg.SUCCESS


def load_known_bank():

    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if home_directory == "":
        print("Home directory not specified, please enter the path to game directory.")
        return model.data_base.Msg.ERROR

    '''
    Load the bank of known images present in the data base.
    Remove the title row of the file.
    '''
    filename_bank = home_directory + library_directory + "/data_bank_known.csv"
    data_bank = model.file_utils.read_from_file(filename_bank)

    if not data_bank:
        return model.data_base.Msg.ERROR

    model.data_base.GameData.known_bank = data_bank[1:]

    return model.data_base.Msg.SUCCESS


def load_novel_bank():
    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if home_directory == "":
        print("Home directory not specified, please enter the path to game directory.")
        return model.data_base.Msg.ERROR

    '''
    Load the bank of novel images present in the data base.
    Remove the title row of the file.
    '''
    filename_bank = home_directory + library_directory + "/data_bank_novel.csv"
    data_bank = model.file_utils.read_from_file(filename_bank)

    if not data_bank:
        return model.data_base.Msg.ERROR

    model.data_base.GameData.novel_bank = data_bank[1:]

    return model.data_base.Msg.SUCCESS


def load_online_data(player_id):

    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if home_directory == "":
        return model.data_base.Msg.ERROR

    filename = home_directory + library_directory + "/data_player_info.csv"
    data = model.file_utils.read_from_file(filename)

    data = data[1:]

    # TODO: IF REQUIRED, re-implement the code based on how the data is stored in the online file.
    for row in data:
        if row[1] == player_id:
            temp_list = [int(s) for s in row[2].split() if s.isdigit()]
            model.data_base.UserInfo.PLAYER_AGE = str(temp_list[0])

            online_data = []
            for i in range(3, len(row)):
                temp_str = row[i].replace(';', ',')
                word_list = temp_str.split(',')
                for j in word_list:
                    online_data.append(j.strip())

            model.data_base.GameData.online_data = online_data
            return model.data_base.Msg.SUCCESS

    return model.data_base.Msg.ERROR


# Checks if the general data exists as expected
# Uses the player's age to identify what data is to be used in the game for the current player
def load_known_data():

    player_age = int(model.data_base.UserInfo.PLAYER_AGE)
    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if player_age == -1 or 48 < player_age or player_age < 17:
        print("Unable to load player age. Please restart the game...")
        return model.data_base.Msg.ERROR

    if home_directory == "":
        print("Home directory not specified, please enter the path to game directory.")
        return model.data_base.Msg.ERROR

    '''
    Retrieve the information of the user based on the age the user has entered.
    Remove the title row of the file.
    '''
    filename = home_directory + library_directory + "/data_set_known.csv"
    data = model.file_utils.read_from_file(filename)

    if not data:
        return model.data_base.Msg.ERROR

    known_data = []
    practice_known_data = []
    column_num = player_age - 11

    if 17 <= player_age < 34:
        model.data_base.GameData.low_freq_lower = 0.4
        model.data_base.GameData.mid_freq_lower = 0.55
        model.data_base.GameData.high_freq_lower = 0.70
        model.data_base.GameData.high_freq_upper = 0.80

        for row in data[1:]:
            if 0.4 <= float(row[column_num]) <= 0.80:
                name = row[2]
                freq = round(float(row[column_num]), 5)
                known_data.append([name, freq])
            elif float(row[column_num]) > 0.80:
                name = row[2]
                freq = round(float(row[column_num]), 5)
                practice_known_data.append([name, freq])
    else:
        min_freq = 9999
        max_freq = -9999
        for row in data[1:]:
            freq = round(float(row[column_num]), 5)
            if 0.4 <= float(row[column_num]):
                if freq < min_freq:
                    min_freq = freq
                if freq > max_freq:
                    max_freq = freq

        diff = max_freq - min_freq
        width = min_freq + int(diff / 3)

        model.data_base.GameData.low_freq_lower = min_freq
        model.data_base.GameData.mid_freq_lower = min_freq + width
        model.data_base.GameData.high_freq_lower = max_freq - width
        model.data_base.GameData.high_freq_upper = max_freq

        for row in data[1:]:
            if float(row[column_num]) > model.data_base.GameData.high_freq_lower and len(practice_known_data) < 4:
                name = row[2]
                freq = round(float(row[column_num]), 5)
                practice_known_data.append([name, freq])
                continue

            if min_freq <= float(row[column_num]) <= max_freq:
                name = row[2]
                freq = round(float(row[column_num]), 5)
                known_data.append([name, freq])

    model.data_base.GameData.known_data = known_data[:]
    model.data_base.GameData.practice_known_data = practice_known_data[:]

    return model.data_base.Msg.SUCCESS


# Checks if all the known image and sound files that are to be used in the game exist and are usable
def validate_sound_bank():

    home_directory = model.data_base.GameData.home_directory
    sounds_directory = model.data_base.GameData.sounds_directory

    for row in model.data_base.GameData.sound_bank:

        if len(row) < 2:
            print("Error in the sound bank. Validation failed.")
            return model.data_base.Msg.ERROR

        sound_path_primary = home_directory + sounds_directory + str(row[1])
        sound_path_secondary = ""
        sound_path_tertiary = ""
        if len(row) > 2:
            sound_path_secondary = home_directory + sounds_directory + str(row[2])
        if len(row) > 3:
            sound_path_tertiary = home_directory + sounds_directory + str(row[3])

        if not (os.path.exists(sound_path_primary)):
            print(str(row[1]).lower() + " : not found in the sound bank")
            # return model.data_base.Msg.ERROR

        if len(row) > 2:
            if not (os.path.exists(sound_path_secondary)):
                print(str(row[2]).lower() + " : not found in the sound bank")
                # return model.data_base.Msg.ERROR

        if len(row) > 3:
            if not (os.path.exists(sound_path_tertiary)):
                print(str(row[3]).lower() + " : not found in the sound bank")
                # return model.data_base.Msg.ERROR

    for row in model.data_base.GameData.carriers_bank:

        if len(row) < 2:
            print("Error in the sound bank. Validation failed.")
            return model.data_base.Msg.ERROR

        sound_path_primary = home_directory + sounds_directory + str(row[1])

        if not (os.path.exists(sound_path_primary)):
            print(str(row[1]).lower() + " : not found in the carrier sound bank")
            # return model.data_base.Msg.ERROR

    for row in model.data_base.GameData.transitions_bank:

        if len(row) < 1:
            print("Error in the sound bank. Validation failed.")
            return model.data_base.Msg.ERROR

        sound_path_primary = home_directory + sounds_directory + str(row[0])

        if not (os.path.exists(sound_path_primary)):
            print(str(row[0]).lower() + " : not found in the transition sound bank")
            # return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


# Checks if all the known image and sound files that are to be used in the game exist and are usable
def validate_known_bank():

    home_directory = model.data_base.GameData.home_directory
    images_directory = model.data_base.GameData.images_directory

    for row in model.data_base.GameData.known_bank:

        if len(row) < 2:
            print("Error in the known bank. Validation failed.")
            return model.data_base.Msg.ERROR

        image_known_path = home_directory + images_directory + str(row[1])

        if not (os.path.exists(image_known_path)):
            print(str(row[1]).lower() + " : not found in the known bank")
            # return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def validate_novel_bank():

    home_directory = model.data_base.GameData.home_directory
    images_directory = model.data_base.GameData.images_directory

    for row in model.data_base.GameData.novel_bank:

        if len(row) < 2:
            print("Error in the novel bank. Validation failed.")
            return model.data_base.Msg.ERROR

        novel_image_path = home_directory + images_directory + str(row[1])

        if not (os.path.exists(novel_image_path)):
            print(str(row[0]).lower() + " : not found in the novel data bank.")
            # return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def validate_known_data():

    if model.data_base.GameData.known_bank == "":
        print("Empty known bank detected. Validation failed.")
        return model.data_base.Msg.ERROR

    for row in model.data_base.GameData.known_data:

        if len(row) < 1:
            print("Error in the known data. Validation failed.")
            return model.data_base.Msg.ERROR

        flag = False
        for bank_row in model.data_base.GameData.known_bank:
            if str(bank_row[0]).lower() == str(row[0]).lower():
                flag = True
                break

        if not flag:
            print(str(row[0]).lower() + " : not found in the known data")
            # return model.data_base.Msg.ERROR

    for row in model.data_base.GameData.practice_known_data:

        if len(row) < 1:
            print("Error in the known data. Validation failed.")
            return model.data_base.Msg.ERROR

        flag = False
        for bank_row in model.data_base.GameData.known_bank:
            if str(bank_row[0]).lower() == str(row[0]).lower():
                flag = True
                break

        if not flag:
            print(str(row[0]).lower() + " : not found in the practice known data")
            # return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def validate_id(alpha_numeric_id):
    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if home_directory == "":
        print("Home directory not specified, please restart the game...")
        return model.data_base.Msg.ERROR

    filename = home_directory + library_directory + "/players_info.csv"
    data = model.file_utils.read_from_file(filename)

    if not data:
        return model.data_base.Msg.ERROR

    for row in data:
        if row[0] == str(alpha_numeric_id):
            return model.data_base.Msg.ERROR

    return model.data_base.Msg.SUCCESS


def generate_output_file(filename):
    home_directory = model.data_base.GameData.home_directory
    outputs_directory = model.data_base.GameData.outputs_directory

    file_path = home_directory + outputs_directory + "/" + filename
    model.file_utils.create_file(file_path)


def upload_id(tag):
    home_directory = model.data_base.GameData.home_directory
    library_directory = model.data_base.GameData.library_directory

    if home_directory == "":
        print("Home directory not specified, please restart the game...")
        return model.data_base.Msg.ERROR

    filename = home_directory + library_directory + "/players_info.csv"
    tag = [str(tag)]
    model.file_utils.write_row_to_file(filename, tag)


def upload_trial_data(filename, row_data):
    home_directory = model.data_base.GameData.home_directory
    outputs_directory = model.data_base.GameData.outputs_directory

    output_file_path = home_directory + outputs_directory + "/" + filename

    if not (os.path.exists(output_file_path)):
        return model.data_base.Msg.ERROR

    model.file_utils.write_row_to_file(output_file_path, row_data)
    return model.data_base.Msg.SUCCESS
