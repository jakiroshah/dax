################################################################################################
# Created: 29th December, 2017
# Author : Aalap Shah
# E-mail : ashah78@jhu.edu
################################################################################################
# Stores the information about the current player and specifications for the game in general
#
# As the code does not intend to have multiple instances of any of these classes, everything can
# be considered to be static.
################################################################################################


class UserInfo:
    PLAYER_ID = ""
    PLAYER_AGE = -1


class GameData:

    application = ""
    start_screen = ""
    primary_screen = ""
    secondary_screen = ""

    carriers_bank = ""
    transitions_bank = ""
    sound_bank = ""
    known_bank = ""
    novel_bank = ""

    online_data = ""
    known_data = ""
    practice_known_data = ""

    paired_data = ""

    low_freq_lower = 0
    mid_freq_lower = 0
    high_freq_lower = 0
    high_freq_upper = 0

    # This is set by the user
    home_directory = ""

    # This is either static or automatically set
    library_directory = "/library"
    outputs_directory = "/library/outputs"
    images_directory = "/library/images/"
    sounds_directory = "/library/sounds/"

    # the total trials per block in a complete version of the game
    known_per_block = 18

    # known-known : known-novel : novel-known
    ideal_data_proportion = [6, 3, 3]

    # -------------------------- PARAMETERS --------------------------
    carrier_filename = ""
    is_player_data_ignored = ""
    image_parameters = ""


class ImageClassification:
    # At least one option in each of these sections must be selected - i.e. must be true

    # ------------- SOURCES -------------
    SOURCE_HALBERDA = True
    SOURCE_GOOGLE = True
    SOURCE_BOSS = True

    # ------------- DIMENSION -------------
    TWO_DIMENSION = False
    THREE_DIMENSION = True

    # ------------- TYPES -------------
    COMPUTER_CARTOON = True
    RENDERED = True
    DRAWING = True
    REAL = True
    CARTOON = True

    # ------------- SHADOW -------------
    SHADOW = False

    # ------------- SHADING -------------
    SHADING = True

    # ------------- COLOR -------------
    COLORED = True
    GREY_SCALE = False
    BLACK_WHITE = False


class ImageType:
    KNOWN = "KNOWN"
    NOVEL = "NOVEL"

    def __init__(self, image_type):
        self.image_type = image_type


class ImageData:

    def __init__(self, object_name, frequency, image_filename, sound_filename, image_type):
        self.object_name = object_name
        self.frequency = frequency
        self.image_filename = image_filename
        self.sound_filename = sound_filename
        self.image_type = image_type

    def set_sound_filename(self, sound_filename):
        self.sound_filename = sound_filename

    def get_sound_filename(self):
        return self.sound_filename

    def get_image_filename(self):
        return self.image_filename

    def get_frequency(self):
        return self.frequency

    def get_object_name(self):
        return self.object_name

    def get_image_type(self):
        return self.image_type


class TrialData:

    def __init__(self, target_name, other_name, target_freq, target_filename, other_filename, target_sound, image_type):
        self.target_name = target_name
        self.other_name = other_name
        self.target_frequency = target_freq
        self.target_filename = target_filename
        self.other_filename = other_filename
        self.target_sound = target_sound
        self.image_type = image_type

    def get_target_name(self):
        return self.target_name
    
    def get_other_name(self):
        return self.other_name

    def get_target_frequency(self):
        return self.target_frequency

    def get_target_filename(self):
        return self.target_filename

    def get_other_filename(self):
        return self.other_filename

    def get_target_sound(self):
        return self.target_sound

    def get_image_type(self):
        return self.image_type
    
    
class BlockData:
    def __init__(self, left_image_path, right_image_path, sounds_path_list):
        self.left_image_path = left_image_path
        self.right_image_path = right_image_path
        self.sounds_path_list = sounds_path_list
        
    def get_left_image_path(self):
        return self.left_image_path

    def get_right_image_path(self):
        return self.right_image_path

    def get_sounds_path_list(self):
        return self.sounds_path_list


class Msg:
    SUCCESS = "SUCCESS"
    ERROR = "ERROR"
