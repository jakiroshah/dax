import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QFileDialog, QHBoxLayout, QVBoxLayout, QDialog, \
                            QPushButton, QGridLayout, QLineEdit, QMessageBox, QDesktopWidget
from PyQt5.QtGui import QPixmap
from PyQt5.QtCore import Qt, QUrl, QTimer
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer

import random


class StartScreen(QDialog):

    def __init__(self, parent=None):
        super(StartScreen, self).__init__(parent)

        self.directory_text = ""
        self.main_layout = QVBoxLayout()

        self.init_gui()

    def init_gui(self):
        directory_label = QLabel("Home directory:")
        directory_edit = QLineEdit()
        directory_edit.setText(self.directory_text)

        directory_button = QPushButton('Browse', self)
        directory_button.clicked.connect(self.directory_button_is_pressed)

        done_button = QPushButton('Done', self)
        done_button.clicked.connect(self.done_button_is_pressed)

        cancel_button = QPushButton('Exit', self)
        cancel_button.clicked.connect(self.close)

        directory_layout = QGridLayout()
        directory_layout.addWidget(directory_label, 0, 0)
        directory_layout.addWidget(directory_edit, 1, 0)
        directory_layout.addWidget(directory_button, 1, 8)

        buttons_layout = QHBoxLayout()
        buttons_layout.addStretch(1)
        buttons_layout.addWidget(done_button)
        buttons_layout.addWidget(cancel_button)

        self.main_layout.addLayout(directory_layout)
        self.main_layout.addLayout(buttons_layout)
        self.setLayout(self.main_layout)

    def directory_button_is_pressed(self):
        self.clear_layout(self.main_layout)
        directory = QFileDialog.getExistingDirectory(None, 'Select the home directory:')

        if directory:
            self.directory_text = directory
        print(directory)
        self.init_gui()

    def clear_layout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.setParent(None)
                else:
                    self.clear_layout(item.layout())

    @staticmethod
    def done_button_is_pressed():
        print("Starting game...")


class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 image'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
    #     self.init_start_ui()
    #
    # def init_start_ui(self):
    #
    #     done_button = QPushButton('Done', self)
    #     if done_button.isChecked():
    #         print("Verifying the inputs...")
    #
    #     cancel_button = QPushButton('Cancel', self)
    #     if cancel_button.isChecked():
    #         print("Exiting game...")
    #
    #
    #
    #     #
    #     # directory = QFileDialog.getExistingDirectory(None, 'Select the home directory:', QFileDialog.ShowDirsOnly)
    #     # print(directory)

    def initUI(self):
        self.setWindowTitle(self.title)
        # self.setGeometry(self.left, self.top, self.width, self.height)
        self.showFullScreen()
        # Create widget
        label = QLabel(self)
        pixmap = QPixmap('/home/jakiroshah/PycharmProjects/panipuri/library/images/b.png')
        label.setPixmap(pixmap)
        self.resize(pixmap.width(), pixmap.height())

        self.show()


class MainScreen(QDialog):
    def __init__(self, filename):
        super(MainScreen, self).__init__()

        self.current_timer = None

        self.label = QLabel(self)

        self.setWindowTitle("Image Viewer")
        self.setStyleSheet("background-color: black;")
        self.resize(800, 800)

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)
        self.showFullScreen()

        self.open_image(filename)
        self.show()

    def start_timer(self):
        if self.current_timer:
            self.current_timer.stop()
            self.current_timer.deleteLater()
        self.current_timer = QTimer()
        self.current_timer.timeout.connect(self.print_hello)
        self.current_timer.setSingleShot(True)
        self.current_timer.start(3000)

    def open_image(self, filename):
        p_map = QPixmap(filename)
        p_map = p_map.scaled(self.width(), self.height())

        self.label.setPixmap(p_map)
        self.centre_image(self.label)
        self.label.show()

    def centre_image(self, label):
        resolution = QDesktopWidget().screenGeometry()

        width = resolution.width() / 2 - self.width() / 2
        height = resolution.height() / 2 - self.height() / 2

        label.setGeometry(width, height, self.width(), self.height())


def play_sound(sound_filename):

    url = QUrl.fromLocalFile(sound_filename)
    content = QMediaContent(url)
    player = QMediaPlayer()
    player.setMedia(content)
    # player.setVolume(100)
    player.play()
    return player


class Playlist:
    def __init__(self):
        self.player_list = []
        self.player = ""

    def play_sound(self, time, index):
        if not self.player_list:
            return

        # self.player = self.player_list[index]
        #
        # print(self.player)
        QTimer.singleShot(time, lambda: self.player_list[index].play())
        return self.player_list[index]

    def add_sound(self, sound_filename):
        url = QUrl.fromLocalFile(sound_filename)
        content = QMediaContent(url)
        player = QMediaPlayer()
        player.setMedia(content)
        # player.play()
        # player.setVolume(100)
        self.player_list.append(player)


if __name__ == '__main__':
    # app = QApplication(sys.argv)
    # ex = MainScreen('/home/jakiroshah/PycharmProjects/panipuri/library/images/b.png')

    # player1 = Playlist()
    # player1.add_sound('/home/jakiroshah/PycharmProjects/panipuri/library/sounds/d.mp3')
    # player1.add_sound('/home/jakiroshah/PycharmProjects/panipuri/library/sounds/e.mp3')

    # QTimer.singleShot(1000, lambda: player1.play_sound)
    # QTimer.singleShot(50000, lambda: player1.play_sound)

    # tempPlayer = player1.play_sound(1000, 0)
    # tempPlayer1 = player1.play_sound(7000, 1)

    # sys.exit(app.exec_())
    # data = [[0, 1, 2], [3, 4, 5]]
    # for row in data:
    #     print(row)
    #     print(row[0])
    # data_lib.load_library()
    # print(data_lib.validate_library())
    # get intersection of known_data and online_data needs to be done

    # known_data = [["/ball.jpg", 0.79, "/ballpoint.mp3", "/ball.mp3"], ["/car".jpg, 0.47, "/car.mp3"],
    #               ["/apple.png", 0.43, "/apple.mp3"], ["/balloon.jpg", 0.40, "/balloonlook2.mp3", "/balloon.mp3"]]
    #
    # novel_data = ["/bazooka_b.jpg", "/aircompressor_b.jpg", "/boatmotor_b.jpg", "/cabasa_b.jpg", "/glark_h.jpg"]
    #
    # filtered_known_data = []
    # filtered_novel_data = []
    #
    # length = 18
    # buckets = [6, 3, 3]
    #
    # # Get a subset of known_data
    # if len(known_data) < model.data_base.GameData.ideal_data_size:
    #     length = len(known_data)
    #
    #     if length <= 3:
    #         print("ERROR")
    #         exit()
    #
    #     prop_sum = buckets[0] + buckets[1] + buckets[2]
    #     bucket1 = int(length * buckets[0] / prop_sum)
    #     bucket2 = int(length * buckets[1] / prop_sum)
    #     bucket3 = int(length * buckets[2] / prop_sum)
    #
    #     if not 2*bucket1 + bucket2 + bucket3 == length:
    #         print("ERROR")
    #         exit()
    #
    #     buckets = [bucket1, bucket2, bucket3]
    #     filtered_known_data = known_data
    # else:
    #     low_frequency = []
    #     mid_frequency = []
    #     high_frequency = []
    #     for row in known_data:
    #         if 0.4 <= row[1] < 0.55:
    #             low_frequency.append(row)
    #         elif 0.55 <= row[1] < 0.7:
    #             mid_frequency.append(row)
    #         elif 0.7 <= row[1] <= 0.81:
    #             high_frequency.append(row)
    #
    #     sorted_list = [low_frequency, mid_frequency, high_frequency]
    #     sorted_list.sort(key=len)
    #
    #     num = int(length / 3)
    #
    #     if len(sorted_list[0]) >= num:
    #         indices = random.sample(range(0, len(sorted_list[0])), num)
    #         for i in indices:
    #             filtered_known_data.append(sorted_list[0][i])
    #
    #         indices = random.sample(range(0, len(sorted_list[1])), num)
    #         for i in indices:
    #             filtered_known_data.append(sorted_list[1][i])
    #
    #         indices = random.sample(range(0, len(sorted_list[2])), length - len(filtered_known_data))
    #         for i in indices:
    #             filtered_known_data.append(sorted_list[1][i])
    #     else:
    #         filtered_known_data.extend(sorted_list[0])
    #
    #         if len(sorted_list[1]) + len(sorted_list[0]) < 2 * num:
    #             filtered_known_data.extend(sorted_list[1])
    #         else:
    #             indices = random.sample(range(0, len(sorted_list[1])), num)
    #             for i in indices:
    #                 filtered_known_data.append(sorted_list[1][i])
    #
    #         if len(sorted_list[2]) < length - len(filtered_known_data):
    #             filtered_known_data.extend(sorted_list[2])
    #         else:
    #             indices = random.sample(range(0, len(sorted_list[2])), length - len(filtered_known_data))
    #             for i in indices:
    #                 filtered_known_data.append(sorted_list[1][i])
    #
    # # Get a subset of novel_data
    # indices = random.sample(range(0, len(novel_data) - 1), length)
    # for i in indices:
    #     filtered_novel_data.append(i)

    foo = []
    foo.append([1, 2, 3])
    foo.append([4, 5, 6])
    print(foo)

    combined = list(zip(foo[0], foo[1]))
    random.shuffle(combined)

    foo[0][:], foo[1][:] = zip(*combined)

    print(foo)
