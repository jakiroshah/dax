# DAX PROJECT

This is a game developed for the Vision and Cognition Lab to identify how young
children develop logic, how early(by age) and with what ease.

## Download the game
Via git:
> Open the terminal `ctrl + alt + t` and type the following commands:
>```sh
>cd ~
>
>git clone https://git.lcsr.jhu.edu/ashah78/panipuri.git
>```
>You will be asked the enter the username and password. Once you are the the folder the appear in the home directory.
You can access it by clicking on the 'folder icon' that appears on the left side panel which says 'files'.

Via a pendrive:
> After you insert the pendrive, copy the `dax` folder into the `Home` directory of ubuntu

## Add a new participant

To add a new participant, simply run the game (described below) and click on
`Generate New ID`. This will update the local database with knowledge that
such a new participant exists. You can check if the ID has been registered by looking
into the `players_info.csv` file present in `~/dax/library`

## Before you start a game

#### Setup:

From the ubuntu search icon (the uppermost icon on the panel to the left):
* **Type `display` and select `Displays`:**
    * Check if there are a total of 3 screens visible!
    * Click on the `Built-in Display` and turn it off. This should make the other two displays available
    * Upon clicking `Apply` remember to select `Keep this configuration`

* **Type `lock` and select `Brightness & Lock`:**
Ensure that the `Turn screen off when inactive for` is set to `Never`

* **Type `power` and select `Power`**:
Ensure that the `Suspend when inactive for` is set to `Don't suspend`

#### Refresh participants list in the database:

It is important that the database is updated every time the game is played. To
do so:
* Download the `.csv` file from the google forms
* Rename the file to `data_player_info.csv`
* Paste and replace the file in the `~/dax/library` directory

Before running the game, you can also check the `players_info.csv` file
present in `~/dax/library` to look for all the registered IDs.

## Run the game

Open the terminal `ctrl + alt + t` and type the following commands:
```sh
cd ~/dax

./dist/main_presenter/main_presenter
```

This will start the game as is expected. If you encounter any error, type in the following commands into the terminal:
```sh

pip3 install pyinstaller

cd ~/dax

pyinstaller --hidden-import=PyQt5.QtMultimedia presenter/main_presenter.py

./dist/main_presenter/main_presenter
```


If the game loads correctly, you should see a GUI!

## Issues

If the game outputs an error namely "GStreamer installation missing":

```sh
sudo apt-get install ubuntu-restricted-extras
```

Also go through the set up for identifying screen over usb here:
https://support.displaylink.com/knowledgebase/articles/684649-how-to-install-displaylink-software-on-ubuntu

---

Whenever new images are put into the images directory:
- Open the terminal `ctrl + alt + t` and type the following commands:
```sh
cd ~/dax/library/images

mogrify -background white -flatten *.png
```

This cleans up all the .png images and does not show the error we were facing anymore. This also ensures the the png images are read correctly when they are being rendered in the game.

- - - -