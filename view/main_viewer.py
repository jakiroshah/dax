################################################################################################
# Created: 1st January, 2018
# Author : Aalap Shah
# E-mail : ashah78@jhu.edu
################################################################################################
# Acts as the view of the game. All screens are included as individual classes
#
# This only communicates with the presenter
################################################################################################

import sys
from PyQt5.QtWidgets import QApplication, QWidget, QLabel, QFileDialog, QHBoxLayout, QVBoxLayout, QDialog, \
                            QPushButton, QGridLayout, QLineEdit, QMessageBox, QDesktopWidget, QComboBox, QCheckBox, \
                            QMainWindow
from PyQt5.QtGui import QPixmap, QMovie
from PyQt5.QtCore import Qt, QUrl, QTimer, QPoint
from PyQt5.QtMultimedia import QMediaContent, QMediaPlayer, QMediaPlaylist

import presenter.main_presenter


# This is the first screen which appears to the user.
# It asks the user for inputs which give an id to the user and helps determine the database that is used in the game
class StartScreen(QDialog):

    def __init__(self, parent=None):
        super(StartScreen, self).__init__(parent)

        self.directory_text = str("/home/jakiroshah/PycharmProjects/dax")
        self.user_id = str("ABCIJK90QR")

        self.id_edit = QLineEdit()

        self.main_layout = QVBoxLayout()

        self.init_gui()

    def init_gui(self):

        id_label = QLabel("Player ID:")
        self.id_edit.setText(self.user_id)

        check_button = QPushButton('Image/Sound Check', self)
        check_button.clicked.connect(self.check_button_is_pressed)

        generator_button = QPushButton('Generate New ID', self)
        generator_button.clicked.connect(self.generator_button_is_pressed)

        directory_label = QLabel("Home directory:")
        directory_edit = QLineEdit()
        directory_edit.setText(self.directory_text)

        directory_button = QPushButton('Browse', self)
        directory_button.clicked.connect(self.directory_button_is_pressed)

        done_button = QPushButton('Done', self)
        done_button.clicked.connect(self.done_button_is_pressed)

        cancel_button = QPushButton('Exit', self)
        cancel_button.clicked.connect(self.close)

        check_layout = QGridLayout()
        check_layout.addWidget(check_button, 0, 0)

        generator_layout = QGridLayout()
        generator_layout.addWidget(generator_button, 0, 0)

        id_layout = QGridLayout()
        id_layout.addWidget(id_label, 0, 0)
        id_layout.addWidget(self.id_edit, 1, 0)

        directory_layout = QGridLayout()
        directory_layout.addWidget(directory_label, 0, 0)
        directory_layout.addWidget(directory_edit, 1, 0)
        directory_layout.addWidget(directory_button, 1, 4)

        buttons_layout = QHBoxLayout()
        buttons_layout.addStretch(1)
        buttons_layout.addWidget(done_button)
        buttons_layout.addWidget(cancel_button)

        self.main_layout.addSpacing(5)
        self.main_layout.addLayout(check_layout)
        self.main_layout.addSpacing(5)
        self.main_layout.addLayout(generator_layout)
        self.main_layout.addSpacing(5)
        self.main_layout.addLayout(directory_layout)
        self.main_layout.addSpacing(10)
        self.main_layout.addLayout(id_layout)
        self.main_layout.addLayout(buttons_layout)
        self.setLayout(self.main_layout)

    def check_button_is_pressed(self):
        if len(self.directory_text) < 1:
            error_gui("Please enter the path to the home directory!")
        else:
            if presenter.main_presenter.set_home_directory(self.directory_text):
                self.close()
                presenter.main_presenter.init_check_screen()

    def generator_button_is_pressed(self):

        if len(self.directory_text) < 1:
            error_gui("Please enter the path to the home directory!")
        else:
            if presenter.main_presenter.set_home_directory(self.directory_text):
                new_id = presenter.main_presenter.generate_new_id()
                MessageScreen(QMessageBox.Information, "The New ID is: ", new_id)

    def directory_button_is_pressed(self):

        self.clear_layout(self.main_layout)

        directory = QFileDialog.getExistingDirectory(None, 'The home directory:', "", QFileDialog.ShowDirsOnly)
        if directory:
            self.directory_text = directory

        self.init_gui()

    def clear_layout(self, layout):
        if layout is not None:
            while layout.count():
                item = layout.takeAt(0)
                widget = item.widget()
                if widget is not None:
                    widget.setParent(None)
                else:
                    self.clear_layout(item.layout())

    # Validates the user input. If the input is valid, sends the information to the presenter
    def done_button_is_pressed(self):

        self.user_id = self.id_edit.text()

        if len(self.user_id) < 1 or len(self.directory_text) < 1:
            error_gui("Invalid input! Please re-enter the data")
        else:
            print("")
            print("Exiting start screen...")
            print("Initializing the game...")
            if presenter.main_presenter.set_user_input(self.directory_text, self.user_id):
                self.close()
                init_second_start_screen()


class SecondStartScreen(QDialog):
    def __init__(self, directory_name, user_id, carrier_filenames, image_parameters, parent=None):
        super(SecondStartScreen, self).__init__(parent)

        self.directory_text = directory_name
        self.user_id = user_id

        self.carrier_name = ""
        self.carrier_filenames = carrier_filenames

        self.parameters = image_parameters
        self.sources = 0
        self.dimensions = 1
        self.types = 2
        self.shadow = 3
        self.shading = 4
        self.color = 5

        self.carrier_drop_down = QComboBox()
        self.dimensions_drop_down = QComboBox()

        self.sources_1 = QCheckBox("HALBERDA")
        self.sources_2 = QCheckBox("GOOGLE")
        self.sources_3 = QCheckBox("BOSS")

        self.types_1 = QCheckBox("COMPUTER_CARTOON")
        self.types_2 = QCheckBox("RENDERED")
        self.types_3 = QCheckBox("DRAWING")
        self.types_4 = QCheckBox("REAL")
        self.types_5 = QCheckBox("CARTOON")

        self.shadow_1 = QCheckBox("SHADOW")

        self.shading_1 = QCheckBox("SHADING")

        self.color_1 = QCheckBox("COLORED")
        self.color_2 = QCheckBox("GREY_SCALE")
        self.color_3 = QCheckBox("BLACK_WHITE")

        self.main_layout = QVBoxLayout()

        self.init_gui()

    def init_gui(self):

        carrier_label = QLabel("Carrier Name:")
        self.populate_carrier_drop_down()
        self.carrier_drop_down.activated[str].connect(self.carrier_is_selected)

        dimensions_label = QLabel("Dimensions:")
        self.dimensions_drop_down.addItem("TWO_DIMENSION")
        self.dimensions_drop_down.addItem("THREE_DIMENSION")

        self.populate_parameters()

        sources_label = QLabel("Sources:")
        self.sources_1.toggled.connect(self.sources_checkbox_is_toggled)
        self.sources_2.toggled.connect(self.sources_checkbox_is_toggled)
        self.sources_3.toggled.connect(self.sources_checkbox_is_toggled)

        self.dimensions_drop_down.activated[str].connect(self.dimension_is_selected)

        types_label = QLabel("Image Types:")
        self.types_1.toggled.connect(self.types_checkbox_is_toggled)
        self.types_2.toggled.connect(self.types_checkbox_is_toggled)
        self.types_3.toggled.connect(self.types_checkbox_is_toggled)
        self.types_4.toggled.connect(self.types_checkbox_is_toggled)
        self.types_5.toggled.connect(self.types_checkbox_is_toggled)

        shadow_label = QLabel("Shadow:")
        self.shadow_1.toggled.connect(self.checkbox_is_toggled)

        shading_label = QLabel("Shading:")
        self.shading_1.toggled.connect(self.checkbox_is_toggled)

        color_label = QLabel("Color:")
        self.color_1.toggled.connect(self.colors_checkbox_is_toggled)
        self.color_2.toggled.connect(self.colors_checkbox_is_toggled)
        self.color_3.toggled.connect(self.colors_checkbox_is_toggled)

        carrier_layout = QGridLayout()
        carrier_layout.addWidget(carrier_label, 0, 0)
        carrier_layout.addWidget(self.carrier_drop_down, 1, 0)

        sources_layout = QGridLayout()
        sources_layout.addWidget(sources_label, 0, 0)
        sources_layout.addWidget(self.sources_1, 1, 0)
        sources_layout.addWidget(self.sources_2, 2, 0)
        sources_layout.addWidget(self.sources_3, 1, 1)

        dimensions_layout = QGridLayout()
        dimensions_layout.addWidget(dimensions_label, 0, 0)
        dimensions_layout.addWidget(self.dimensions_drop_down, 1, 0)

        types_layout = QGridLayout()
        types_layout.addWidget(types_label, 0, 0)
        types_layout.addWidget(self.types_1, 1, 0)
        types_layout.addWidget(self.types_2, 2, 0)
        types_layout.addWidget(self.types_3, 3, 0)
        types_layout.addWidget(self.types_4, 1, 1)
        types_layout.addWidget(self.types_5, 2, 1)

        shadow_layout = QGridLayout()
        shadow_layout.addWidget(shadow_label, 0, 0)
        shadow_layout.addWidget(self.shadow_1)

        shading_layout = QGridLayout()
        shading_layout.addWidget(shading_label, 0, 0)
        shading_layout.addWidget(self.shading_1)

        colors_layout = QGridLayout()
        colors_layout.addWidget(color_label, 0, 0)
        colors_layout.addWidget(self.color_1)
        colors_layout.addWidget(self.color_2)
        colors_layout.addWidget(self.color_3)

        done_button = QPushButton('Done', self)
        done_button.clicked.connect(self.done_button_is_pressed)

        cancel_button = QPushButton('Exit', self)
        cancel_button.clicked.connect(self.close)

        buttons_layout = QHBoxLayout()
        buttons_layout.addStretch(1)
        buttons_layout.addWidget(done_button)
        buttons_layout.addWidget(cancel_button)

        self.main_layout.addSpacing(5)
        self.main_layout.addLayout(carrier_layout)
        self.main_layout.addLayout(sources_layout)
        self.main_layout.addLayout(dimensions_layout)
        self.main_layout.addLayout(types_layout)
        self.main_layout.addLayout(shadow_layout)
        self.main_layout.addLayout(shading_layout)
        self.main_layout.addLayout(colors_layout)
        self.main_layout.addLayout(buttons_layout)
        self.setLayout(self.main_layout)

    def populate_carrier_drop_down(self):
        for name in self.carrier_filenames:
            self.carrier_drop_down.addItem(str(name))
        if len(self.carrier_filenames) > 0:
            self.carrier_name = self.carrier_filenames[0]

    def populate_parameters(self):
        self.sources_1.setChecked(self.parameters[self.sources][0])
        self.sources_2.setChecked(self.parameters[self.sources][1])
        self.sources_3.setChecked(self.parameters[self.sources][2])

        if self.parameters[self.dimensions][0]:
            self.dimensions_drop_down.setCurrentIndex(0)
        else:
            self.dimensions_drop_down.setCurrentIndex(1)

        self.types_1.setChecked(self.parameters[self.types][0])
        self.types_2.setChecked(self.parameters[self.types][1])
        self.types_3.setChecked(self.parameters[self.types][2])
        self.types_4.setChecked(self.parameters[self.types][3])
        self.types_5.setChecked(self.parameters[self.types][4])

        self.shadow_1.setChecked(self.parameters[self.shadow][0])

        self.shading_1.setChecked(self.parameters[self.shading][0])

        self.color_1.setChecked(self.parameters[self.color][0])
        self.color_2.setChecked(self.parameters[self.color][1])
        self.color_3.setChecked(self.parameters[self.color][2])

    def checkbox_is_toggled(self):

        if self.shadow_1.isChecked():
            self.parameters[self.shadow][0] = True
        if not self.shadow_1.isChecked():
            self.parameters[self.shadow][0] = False

        if self.shading_1.isChecked():
            self.parameters[self.shading][0] = True
        if not self.shading_1.isChecked():
            self.parameters[self.shading][0] = False

    def sources_checkbox_is_toggled(self):

        temp = self.parameters[self.sources][:]

        if self.sources_1.isChecked():
            self.parameters[self.sources][0] = True
        if self.sources_2.isChecked():
            self.parameters[self.sources][1] = True
        if self.sources_3.isChecked():
            self.parameters[self.sources][2] = True

        if not self.sources_1.isChecked():
            self.parameters[self.sources][0] = False

        if not self.sources_2.isChecked():
            self.parameters[self.sources][1] = False

        if not self.sources_3.isChecked():
            self.parameters[self.sources][2] = False

        num_sum = 0
        for param in self.parameters[self.sources]:
            num_sum = num_sum + param
        if num_sum == 0:
            self.sources_1.setChecked(temp[0])
            self.sources_2.setChecked(temp[1])
            self.sources_3.setChecked(temp[2])
            error_gui("At least on check box in this section must be selected!")

    def types_checkbox_is_toggled(self):

        temp = self.parameters[self.types][:]

        if self.types_1.isChecked():
            self.parameters[self.types][0] = True
        if self.types_2.isChecked():
            self.parameters[self.types][1] = True
        if self.types_3.isChecked():
            self.parameters[self.types][2] = True
        if self.types_4.isChecked():
            self.parameters[self.types][3] = True
        if self.types_5.isChecked():
            self.parameters[self.types][4] = True

        if not self.types_1.isChecked():
            self.parameters[self.types][0] = False

        if not self.types_2.isChecked():
            self.parameters[self.types][1] = False

        if not self.types_3.isChecked():
            self.parameters[self.types][2] = False

        if not self.types_4.isChecked():
            self.parameters[self.types][3] = False

        if not self.types_5.isChecked():
            self.parameters[self.types][4] = False

        num_sum = 0
        for param in self.parameters[self.types]:
            num_sum = num_sum + param
        if num_sum == 0:
            self.types_1.setChecked(temp[0])
            self.types_2.setChecked(temp[1])
            self.types_3.setChecked(temp[2])
            self.types_4.setChecked(temp[3])
            self.types_5.setChecked(temp[4])
            error_gui("At least on check box in this section must be selected!")

    def colors_checkbox_is_toggled(self):

        temp = self.parameters[self.color][:]

        if self.color_1.isChecked():
            self.parameters[self.color][0] = True
        if self.color_2.isChecked():
            self.parameters[self.color][1] = True
        if self.color_3.isChecked():
            self.parameters[self.color][2] = True

        if not self.color_1.isChecked():
            self.parameters[self.color][0] = False

        if not self.color_2.isChecked():
            self.parameters[self.color][1] = False

        if not self.color_3.isChecked():
            self.parameters[self.color][2] = False

        num_sum = 0
        for param in self.parameters[self.color]:
            num_sum = num_sum + param
        if num_sum == 0:
            self.color_1.setChecked(temp[0])
            self.color_2.setChecked(temp[1])
            self.color_3.setChecked(temp[2])
            error_gui("At least on check box in this section must be selected!")

    def dimension_is_selected(self, text):
        if text == "TWO_DIMENSION":
            self.parameters[self.dimensions][0] = True
            self.parameters[self.dimensions][1] = False
        elif text == "THREE_DIMENSION":
            self.parameters[self.dimensions][0] = False
            self.parameters[self.dimensions][1] = True

    def carrier_is_selected(self, text):
        self.carrier_name = text

    def done_button_is_pressed(self):
        if not self.carrier_name == "":
            presenter.main_presenter.set_image_parameters(self.parameters)
            if presenter.main_presenter.set_carrier_filename(self.carrier_name):
                self.close()
                presenter.main_presenter.init_main_screen(self.user_id)


class NewMainScreen(QMainWindow):
    def __init__(self, t_screen, screen_num, bg_image, time_per_bg_image, image_list, time_per_image, parent=None):
        super(NewMainScreen, self).__init__(parent)

        self.temp_screen = t_screen

        self.screen_num = screen_num

        self.background_image = bg_image
        self.time_per_bg_image = time_per_bg_image
        self.image_list = image_list
        self.time_per_image = time_per_image
        self.count = 0

        self.is_image_active = True

        self.screen_origin_x = 0
        self.screen_origin_y = 0
        self.image_origin_x = 0
        self.image_origin_y = 0
        self.image_width = 0
        self.image_height = 0

        self.label = QLabel(self)

        self.prepare_window()

    def prepare_window(self):

        self.setStyleSheet("background-color: white;")

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)

        self.label.setAlignment(Qt.AlignCenter)
        self.setCentralWidget(self.label)

        self.showFullScreen()

    def open_image(self, filename):
        p_map = QPixmap(filename)

        if p_map.width() == 0:
            error_msg = str(filename) + " could not be rendered. Restart the game."
            error_gui(error_msg)
            return

        self.image_width = p_map.width()
        self.image_height = p_map.height()

        self.set_image_params()

        p_map = p_map.scaled(self.image_width, self.image_height)
        self.label.setPixmap(p_map)

        self.label.setGeometry(self.image_origin_x, self.image_origin_y, self.image_width, self.image_height)
        self.move(self.screen_origin_x, self.screen_origin_y)

    def set_image_params(self):

        resolution = QDesktopWidget().screenGeometry(0)

        if self.screen_num == 1:
            if QDesktopWidget().screenCount() == 1 or QDesktopWidget().screenCount() == 2:
                resolution = QDesktopWidget().screenGeometry(0)

            elif QDesktopWidget().screenCount() > 2:
                resolution = QDesktopWidget().screenGeometry(1)

        elif self.screen_num == 2:
            if QDesktopWidget().screenCount() == 1:
                resolution = QDesktopWidget().screenGeometry(0)

            elif QDesktopWidget().screenCount() == 2:
                resolution = QDesktopWidget().screenGeometry(1)

            elif QDesktopWidget().screenCount() > 2:
                resolution = QDesktopWidget().screenGeometry(2)

        ratio = self.image_height / self.image_width

        self.image_width = int(resolution.width() / 3)
        self.image_height = ratio * self.image_width

        if self.image_height > int(4 * resolution.height() / 5):
            self.image_height = int(4 * resolution.height() / 5)
            self.image_width = (1 / ratio) * self.image_height

        self.image_origin_x = resolution.width() / 2 - self.image_width / 2
        self.image_origin_y = resolution.height() / 2 - self.image_height / 2

        self.screen_origin_x = resolution.x()
        self.screen_origin_y = resolution.y()

    def next_image(self):
        if self.image_list and self.count < len(self.image_list) and not self.is_image_active:
            self.open_image(self.image_list[self.count])
            self.count = self.count + 1
            self.is_image_active = True
        elif self.is_image_active:
            self.open_image(self.background_image)
            self.is_image_active = False
        elif self.count == len(self.image_list):
            self.temp_screen.done(0)
            self.close()


class SingleSplitScreen(QMainWindow):
    def __init__(self, t_screen, bg_image, time_per_bg_image, image_list, time_per_image, parent=None):
        super(SingleSplitScreen, self).__init__(parent)

        self.temp_screen = t_screen

        self.background_image = bg_image
        self.time_per_bg_image = time_per_bg_image
        self.image_list_left = image_list[0]
        self.image_list_right = image_list[1]
        self.time_per_image = time_per_image
        self.count = 0

        self.is_image_active = True

        self.screen_origin_x = 0
        self.screen_origin_y = 0
        self.image_origin_x = 0
        self.image_origin_y = 0
        self.image_width = 0
        self.image_height = 0

        self.label_left = QLabel(self)
        self.label_right = QLabel(self)

        self.prepare_window()

    def prepare_window(self):

        self.setStyleSheet("background-color: white;")

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)

        self.label_left.setAlignment(Qt.AlignLeft)
        self.label_right.setAlignment(Qt.AlignRight)
        # self.setCentralWidget(self.label)

        self.showFullScreen()

    def open_image(self, filename, img_pos):
        p_map = QPixmap(filename)

        if p_map.width() == 0:
            error_msg = str(filename) + " could not be rendered. Restart the game."
            error_gui(error_msg)
            return

        self.image_width = p_map.width()
        self.image_height = p_map.height()

        self.set_image_params(img_pos)

        p_map = p_map.scaled(self.image_width, self.image_height)

        if img_pos == "LEFT":
            self.label_left.setPixmap(p_map)
            self.label_left.setGeometry(self.image_origin_x, self.image_origin_y, self.image_width, self.image_height)
        else:
            self.label_right.setPixmap(p_map)
            self.label_right.setGeometry(self.image_origin_x, self.image_origin_y, self.image_width, self.image_height)
        self.move(self.screen_origin_x, self.screen_origin_y)

    def set_image_params(self, img_pos):

        resolution = QDesktopWidget().screenGeometry(0)
        self.setGeometry(resolution)

        ratio = self.image_height / self.image_width

        self.image_width = int(resolution.width() / 6)
        self.image_height = ratio * self.image_width

        if self.image_height > int(2 * resolution.height() / 5):
            self.image_height = int(2 * resolution.height() / 5)
            self.image_width = (1 / ratio) * self.image_height

        if img_pos == "LEFT":
            self.image_origin_x = resolution.x()
            self.image_origin_y = resolution.height() / 2 - self.image_height / 2
        else:
            self.image_origin_x = resolution.width() - self.image_width
            self.image_origin_y = resolution.height() / 2 - self.image_height / 2

        self.screen_origin_x = resolution.x()
        self.screen_origin_y = resolution.y()

    def next_image(self):
        if self.image_list_left and self.count < len(self.image_list_left) and not self.is_image_active:
            self.open_image(self.image_list_left[self.count], "LEFT")
            self.open_image(self.image_list_right[self.count], "RIGHT")
            self.count = self.count + 1
            self.is_image_active = True
        elif self.is_image_active:
            self.open_image(self.background_image, "LEFT")
            self.open_image(self.background_image, "RIGHT")
            self.is_image_active = False
        elif self.count == len(self.image_list_left):
            self.temp_screen.done(0)
            self.close()


class MainScreen(QDialog):
    def __init__(self, filename):
        super(MainScreen, self).__init__()

        self.label = QLabel(self)

        self.setWindowTitle("Image Viewer")
        self.setStyleSheet("background-color: white;")

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)
        self.showFullScreen()

        self.open_image(filename)

    def open_image(self, filename):
        p_map = QPixmap(filename)

        if p_map.width() == 0:
            error_msg = str(filename) + " could not be rendered. Restart the game."
            error_gui(error_msg)
            return

        self.centre_image(self.label, p_map)
        p_map = p_map.scaled(self.width(), self.height())
        self.label.setPixmap(p_map)

    def centre_image(self, label, p_map):
        resolution = QDesktopWidget().screenGeometry()

        if QDesktopWidget().screenCount() == 1 or QDesktopWidget().screenCount() == 2:
            resolution = QDesktopWidget().screenGeometry(0)

        elif QDesktopWidget().screenCount() > 2:
            resolution = QDesktopWidget().screenGeometry(1)

        ratio = p_map.height() / p_map.width()

        new_width = int(resolution.width() / 3)
        new_height = ratio * new_width

        self.resize(new_width, new_height)

        width = resolution.width() / 2 - self.width() / 2
        height = resolution.height() / 2 - self.height() / 2

        label.setGeometry(width, height, self.width(), self.height())
        self.move(QPoint(resolution.x(), resolution.y()))


class SecondScreen(QDialog):
    def __init__(self, filename):
        super(SecondScreen, self).__init__()

        self.label = QLabel(self)

        self.setWindowTitle("Image Viewer")
        self.setStyleSheet("background-color: white;")

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)
        self.showFullScreen()

        self.open_image(filename)

    def open_image(self, filename):
        p_map = QPixmap(filename)

        if p_map.width() == 0:
            error_msg = str(filename) + " could not be rendered. Restart the game."
            error_gui(error_msg)
            return

        self.centre_image(self.label, p_map)
        p_map = p_map.scaled(self.width(), self.height())
        self.label.setPixmap(p_map)

    def centre_image(self, label, p_map):
        resolution = QDesktopWidget().screenGeometry()

        if QDesktopWidget().screenCount() == 1:
            resolution = QDesktopWidget().screenGeometry(0)

        elif QDesktopWidget().screenCount() == 2:
            resolution = QDesktopWidget().screenGeometry(1)

        elif QDesktopWidget().screenCount() > 2:
            resolution = QDesktopWidget().screenGeometry(2)

        ratio = p_map.height() / p_map.width()

        new_width = int(resolution.width() / 3)
        new_height = ratio * new_width

        self.resize(new_width, new_height)

        width = resolution.width() / 2 - self.width() / 2
        height = resolution.height() / 2 - self.height() / 2

        label.setGeometry(width, height, self.width(), self.height())
        self.move(QPoint(resolution.x(), resolution.y()))


class MainMovieScreen(QDialog):
    def __init__(self, filename):
        super(MainMovieScreen, self).__init__()

        self.label = QLabel(self)

        self.setWindowTitle("Image Viewer")
        self.setStyleSheet("background-color: white;")
        self.resize(800, 800)

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)
        self.showFullScreen()

        self.open_image(filename)

    def open_image(self, filename):
        movie = QMovie(filename)

        self.label.setMovie(movie)
        self.centre_image(self.label)
        self.label.setScaledContents(True)

        movie.start()

    def centre_image(self, label):
        resolution = QDesktopWidget().screenGeometry()

        if QDesktopWidget().screenCount() == 1 or QDesktopWidget().screenCount() == 2:
            resolution = QDesktopWidget().screenGeometry(0)
            self.move(QPoint(resolution.x(), resolution.y()))

        elif QDesktopWidget().screenCount() > 2:
            resolution = QDesktopWidget().screenGeometry(1)
            self.move(QPoint(resolution.x(), resolution.y()))

        width = resolution.width() / 2 - self.width() / 2
        height = resolution.height() / 2 - self.height() / 2

        label.setGeometry(width, height, self.width(), self.height())


class SecondMovieScreen(QDialog):
    def __init__(self, filename):
        super(SecondMovieScreen, self).__init__()

        self.label = QLabel(self)

        self.setWindowTitle("Image Viewer")
        self.setStyleSheet("background-color: white;")
        self.resize(800, 800)

        self.setWindowFlags(Qt.FramelessWindowHint)
        self.setWindowState(self.windowState() | Qt.WindowFullScreen)
        self.showFullScreen()

        self.open_image(filename)

    def open_image(self, filename):
        movie = QMovie(filename)

        self.label.setMovie(movie)
        self.centre_image(self.label)
        self.label.setScaledContents(True)

        movie.start()

    def centre_image(self, label):
        resolution = QDesktopWidget().screenGeometry()
        if QDesktopWidget().screenCount() == 1:
            resolution = QDesktopWidget().screenGeometry(0)
            self.move(QPoint(resolution.x(), resolution.y()))
        elif QDesktopWidget().screenCount() == 2:
            resolution = QDesktopWidget().screenGeometry(1)
            self.move(QPoint(resolution.x(), resolution.y()))
        elif QDesktopWidget().screenCount() > 2:
            resolution = QDesktopWidget().screenGeometry(2)
            self.move(QPoint(resolution.x(), resolution.y()))

        width = resolution.width() / 2 - self.width() / 2
        height = resolution.height() / 2 - self.height() / 2

        label.setGeometry(width, height, self.width(), self.height())


class MessageScreen(QWidget):

    def __init__(self, message_type, title, message):
        super().__init__()

        self.move_to_centre()

        error_box = QMessageBox(self)
        error_box.setIcon(message_type)
        error_box.setText(title)

        error_box.setInformativeText(str(message))
        error_box.addButton(QMessageBox.Ok)

        error_box.setDefaultButton(QMessageBox.Ok)
        ret = error_box.exec_()

        if ret == QMessageBox.Ok:
            return

    def move_to_centre(self):
        resolution = QDesktopWidget().screenGeometry()

        width = resolution.width() / 2
        height = resolution.height() / 2

        frame_width = self.frameSize().width() / 2
        frame_height = self.frameSize().height() / 2

        self.move(width - frame_width, height - frame_height)


class BlankScreen(QDialog):

    def __init__(self):
        super().__init__()

    def keyPressEvent(self, e):
        if e.key() == Qt.Key_Space:
            self.close()


class SoundPlayerTrials:
    def __init__(self, sound_list, time_wait, time_sound1, time_sound2, time_frequency):
        self.sound_list = sound_list

        self.time_1 = time_sound1
        self.time_2 = time_sound2
        self.time_frequency = time_frequency

        self.player_list = []
        self.volume = 100

        self.count = 0

    def play_sound(self, start_after_time, index):
        if not self.player_list:
            return

        QTimer.singleShot(start_after_time, lambda: self.player_list[index].play())
        return self.player_list[index]

    def add_sound(self, sound_filename):

        url = QUrl.fromLocalFile(sound_filename)
        content = QMediaContent(url)
        player = QMediaPlayer()
        player.setMedia(content)
        player.setVolume(self.volume)
        self.player_list.append(player)

    def add_sounds(self, sound_filenames):

        player = QMediaPlayer()
        playlist = QMediaPlaylist(player)
        for filename in sound_filenames:
            url = QUrl.fromLocalFile(filename)
            content = QMediaContent(url)
            playlist.addMedia(content)

        playlist.setCurrentIndex(0)
        player.setPlaylist(playlist)
        player.setVolume(self.volume)
        self.player_list.append(player)

    def next_sound(self):
        self.player_list = []

        if self.sound_list and self.count < len(self.sound_list):
            sounds = self.sound_list[self.count]
            if len(sounds) == 1:
                self.add_sound(sounds[0])
            elif len(sounds) == 2:
                self.add_sound(sounds[0])
                self.add_sound(sounds[1])
            elif len(sounds) > 2:
                playlist = [sounds[0], sounds[1]]
                self.add_sounds(playlist)
                self.add_sound(sounds[2])

            self.play_sound(self.time_1, 0)
            if len(sounds) > 2:
                self.play_sound(self.time_2, 1)

            self.count = self.count + 1


class SoundPlayerTransition:
    def __init__(self):
        self.player_list = []
        self.volume = 100

    def play_sound(self, start_after_time, index):
        if not self.player_list:
            return

        QTimer.singleShot(start_after_time, lambda: self.player_list[index].play())
        return self.player_list[index]

    def add_sound(self, sound_filename):

        url = QUrl.fromLocalFile(sound_filename)
        content = QMediaContent(url)
        player = QMediaPlayer()
        player.setMedia(content)
        player.setVolume(self.volume)
        self.player_list.append(player)

    def add_sounds(self, sound_filenames):

        player = QMediaPlayer()
        playlist = QMediaPlaylist(player)
        for filename in sound_filenames:
            url = QUrl.fromLocalFile(filename)
            content = QMediaContent(url)
            playlist.addMedia(content)

        playlist.setCurrentIndex(0)
        player.setPlaylist(playlist)
        player.setVolume(self.volume)
        self.player_list.append(player)


# Displays the error message in the form of a pop-up message box
def error_gui(error_msg):
    MessageScreen(QMessageBox.Critical, "An error occurred!", error_msg)


# Creates the instance for the application for the viewer
def init_view():
    view_application = QApplication.instance()
    if not view_application:
        view_application = QApplication(sys.argv)

    return view_application


# Displays the starting menu screen
def init_start_screen():
    start_screen = StartScreen()
    start_screen.exec_()


def init_second_start_screen():
    home_directory = presenter.main_presenter.get_home_directory()
    user_info = presenter.main_presenter.get_user_data()
    user_id = user_info.PLAYER_ID

    carrier_filenames = presenter.main_presenter.get_carrier_filenames()
    image_parameters = presenter.main_presenter.get_image_parameters()

    if len(carrier_filenames) > 0:
        second_start_screen = SecondStartScreen(home_directory, user_id, carrier_filenames, image_parameters)
        second_start_screen.exec_()


def init_check_view(view_application, bg_screen, all_images, all_sounds):
    view_application.setOverrideCursor(Qt.BlankCursor)

    time_between_images = 1000
    time_per_image = 8000
    time_sound_1 = 3000
    time_sound_2 = 5500
    time_sound_frequency = time_per_image + time_between_images

    next_image_timer = [time_between_images, time_per_image]

    main_screen0 = MainScreen(bg_screen)
    main_screen0.show()
    main_screen0.raise_()

    t_screen = MainScreen(bg_screen)

    main_screen = NewMainScreen(t_screen, 1, bg_screen, time_between_images, all_images, time_per_image)
    main_screen.show()
    main_screen.raise_()

    player = SoundPlayerTrials(all_sounds, time_between_images, time_sound_1, time_sound_2, time_sound_frequency)

    time_elapsed_image = 0
    time_elapsed_sound = time_between_images
    QTimer().singleShot(time_elapsed_image, lambda: main_screen.next_image())
    QTimer().singleShot(time_elapsed_sound, lambda: player.next_sound())
    for _ in range(len(all_images)):

        time_elapsed_sound = time_elapsed_sound + time_sound_frequency
        QTimer().singleShot(time_elapsed_sound, lambda: player.next_sound())
        for i in next_image_timer:
            time_elapsed_image = time_elapsed_image + i
            QTimer().singleShot(time_elapsed_image, lambda: main_screen.next_image())
    QTimer().singleShot(time_elapsed_image + next_image_timer[0], lambda: main_screen.next_image())

    t_screen.exec_()


def run_block(t_screen, bg_screen, left_screen_images, right_screen_images, sound_list):
    # these times define when the described event occurs after the image is first displayed
    time_between_images = 1000
    time_per_image = 8000
    time_sound_1 = 3000
    time_sound_2 = 5500
    time_sound_frequency = time_per_image + time_between_images

    next_image_timer = [time_between_images, time_per_image]

    main_screen = SingleSplitScreen(t_screen, bg_screen, time_between_images,
                                    [left_screen_images, right_screen_images], time_per_image)
    main_screen.show()
    main_screen.raise_()

    player = SoundPlayerTrials(sound_list, time_between_images, time_sound_1, time_sound_2, time_sound_frequency)

    time_elapsed_image = 0
    time_elapsed_sound = time_between_images
    QTimer().singleShot(time_elapsed_image, lambda: main_screen.next_image())
    QTimer().singleShot(time_elapsed_sound, lambda: player.next_sound())
    for _ in range(len(left_screen_images)):
        time_elapsed_sound = time_elapsed_sound + time_sound_frequency
        QTimer().singleShot(time_elapsed_sound, lambda: player.next_sound())
        for i in next_image_timer:
            time_elapsed_image = time_elapsed_image + i
            QTimer().singleShot(time_elapsed_image, lambda: main_screen.next_image())

    QTimer().singleShot(time_elapsed_image + next_image_timer[0], lambda: main_screen.next_image())


def init_main_screen(view_application, bg_screen, tr_screen, practice_blocks, block_trial_list, tr_sound, end_sound):
    view_application.setOverrideCursor(Qt.BlankCursor)

    # time for transition and the end trial blocks
    time_sound_transition = 12000

    trials_per_block = int(len(block_trial_list) / 2)

    main_screen0 = MainScreen(bg_screen)
    main_screen0.show()
    main_screen0.raise_()

    # --------------------------- STOPPING FUNCTION ---------------------------

    view_application.setOverrideCursor(Qt.ArrowCursor)

    temp_screen = MainScreen(bg_screen)
    temp_screen.show()
    temp_screen.raise_()

    temp_screen.exec_()

    view_application.setOverrideCursor(Qt.BlankCursor)

    # --------------------------- PRACTICE BLOCK ---------------------------

    t_screen = MainScreen(bg_screen)
    # t_screen.hide()

    left_screen_images = []
    right_screen_images = []
    sound_list = []
    for trial in practice_blocks:
        left_screen_images.append(trial.get_left_image_path())
        right_screen_images.append(trial.get_right_image_path())
        sound_list.append(trial.get_sounds_path_list())

    run_block(t_screen, bg_screen, left_screen_images, right_screen_images, sound_list)

    t_screen.exec_()

    # --------------------------- STOPPING FUNCTION ---------------------------

    view_application.setOverrideCursor(Qt.ArrowCursor)

    temp_screen = MainScreen(bg_screen)
    temp_screen.show()
    temp_screen.raise_()

    temp_screen.exec_()

    view_application.setOverrideCursor(Qt.BlankCursor)

    # --------------------------- FIRST BLOCK ---------------------------

    t_screen = MainScreen(bg_screen)

    left_screen_images = []
    right_screen_images = []
    sound_list = []
    for trial in block_trial_list[:trials_per_block]:
        left_screen_images.append(trial.get_left_image_path())
        right_screen_images.append(trial.get_right_image_path())
        sound_list.append(trial.get_sounds_path_list())

    run_block(t_screen, bg_screen, left_screen_images, right_screen_images, sound_list)

    t_screen.exec_()

    # --------------------------- TRANSITION ---------------------------
    main_screen0.raise_()

    temp_screen1 = MainMovieScreen(tr_screen)

    player_transition = SoundPlayerTransition()
    if len(tr_sound) == 1:
        player_transition.add_sound(tr_sound[0])
    elif len(tr_sound) == 2:
        player_transition.add_sound(tr_sound[0])
        player_transition.add_sound(tr_sound[1])
    elif len(tr_sound) == 3:
        player_transition.add_sound(tr_sound[0])
        player_transition.add_sound(tr_sound[1])
        player_transition.add_sound(tr_sound[2])

    sound1 = player_transition.play_sound(1000, 0)
    if len(tr_sound) == 2:
        sound2 = player_transition.play_sound(3500, 1)
    if len(tr_sound) == 3:
        sound2 = player_transition.play_sound(3500, 1)
        sound3 = player_transition.play_sound(9500, 2)

    QTimer().singleShot(time_sound_transition, lambda: temp_screen1.done(0))
    temp_screen1.exec_()

    main_screen0.raise_()

    # # --------------------------- SECOND BLOCK ---------------------------

    t_screen = MainScreen(bg_screen)

    left_screen_images = []
    right_screen_images = []
    sound_list = []
    for trial in block_trial_list[trials_per_block:]:
        left_screen_images.append(trial.get_left_image_path())
        right_screen_images.append(trial.get_right_image_path())
        sound_list.append(trial.get_sounds_path_list())

    run_block(t_screen, bg_screen, left_screen_images, right_screen_images, sound_list)

    t_screen.exec_()

    # --------------------------- END GAME ---------------------------
    main_screen0.raise_()

    temp_screen1 = MainMovieScreen(tr_screen)

    player_transition = SoundPlayerTransition()
    if len(end_sound) == 1:
        player_transition.add_sound(end_sound[0])
    elif len(end_sound) == 2:
        player_transition.add_sound(end_sound[0])
        player_transition.add_sound(end_sound[1])

    sound1 = player_transition.play_sound(1000, 0)
    if len(end_sound) == 2:
        sound2 = player_transition.play_sound(3500, 1)

    QTimer().singleShot(time_sound_transition, lambda: temp_screen1.done(0))
    temp_screen1.exec_()

    main_screen0.exec_()


def remove_view(view):
    sys.exit(view.exec_())
